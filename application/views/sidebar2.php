<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CP17 UTT</title>
    <link href="<?php echo base_url("public/template")?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url("public/template")?>/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <style>
    #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
	}

	#myImg:hover {opacity: 0.7;}

	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 100px; /* Location of the box */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
	}

	/* Modal Content (image) */
	.modal-content {
	    margin: auto;
	    display: block;
	    width: 80%;
	    max-width: 700px;
	}

	/* Caption of Modal Image */
	#caption {
	    margin: auto;
	    display: block;
	    width: 80%;
	    max-width: 700px;
	    text-align: center;
	    color: #ccc;
	    padding: 10px 0;
	    height: 150px;
	}

	/* Add Animation */
	.modal-content, #caption {    
	    -webkit-animation-name: zoom;
	    -webkit-animation-duration: 0.6s;
	    animation-name: zoom;
	    animation-duration: 0.6s;
	}

	@-webkit-keyframes zoom {
	    from {-webkit-transform:scale(0)} 
	    to {-webkit-transform:scale(1)}
	}

	@keyframes zoom {
	    from {transform:scale(0)} 
	    to {transform:scale(1)}
	}

	/* The Close Button */
	.close {
	    position: absolute;
	    top: 15px;
	    right: 35px;
	    color: #f1f1f1;
	    font-size: 40px;
	    font-weight: bold;
	    transition: 0.3s;
	}

	.close:hover,
	.close:focus {
	    color: #bbb;
	    text-decoration: none;
	    cursor: pointer;
	}

	/* 100% Image Width on Smaller Screens */
	@media only screen and (max-width: 700px){
	    .modal-content {
	        width: 100%;
	    }
	}
	</style>
</head>


<body>
    <div id="wrapper">
	<nav class="nabar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a class="" href="<?php echo base_url("index.php/pages/home")?>"><i class="fa fa-home"></i> Home</a>
                        </li> 
                        <li>							                            
							<a href="#"><i class="fa fa-list-ul"></i> Admin <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a class="" href="<?php echo base_url("index.php/mahasiswa/")?>/list_mahasiswa"> Mahasiswa</a>
								</li>
								<li>
									<a class="" href="<?php echo base_url("index.php/mahasiswa/")?>/download_mahasiswa"> Download Data Mahasiswa</a>
								</li>
								<li>
									<a class="" href="#"> Download Alamat</a>
								</li>
								<li>
									<a class="" href="#"> Download List Pemohon Ujian Ulang</a>
								</li>
								
							</ul>
                        </li>
						
						<li>							                            
							<a href="#"><i class="fa fa-book"></i> Adakemik <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a class="" href="#"> Pencarian Mahasiswa</a>
								</li>
								<li>
									<a class="" href="<?php echo base_url("index.php/pages/")?>/download_mahasiswa"> Download Data Mahasiswa</a>
								</li>
								
							</ul>
                        </li>

                        <li>							                            
							<a href="#"><i class="fa fa-book"></i> Tutor  <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a class="" href="<?php echo base_url("index.php/tutor/")?>/tutorlist"> List Data Tutor</a>
								</li>
								<li>
									<a class="" href="<?php echo base_url("index.php/pages/")?>/data_tutor"> Download Data Tutor</a>
								</li>
								
							</ul>
                        </li>
						
						
						
						<li>							                            
							<a href="#"><i class="fa fa-money"></i> Bendahara <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a class="download_pembayaran" href="<?php echo base_url("index.php/pembayaran/")?>/homebayar"> Data Pembayaran</a>
								</li>
								<li>
									<a class="" href="<?php echo base_url("index.php/pembayaran/")?>/download_pembayaran"> Download Rekap Pembayaran</a>
								</li>
							</ul>
                        </li>
						
						
						
						<li>
                            <a class="" href="<?php echo base_url("index.php/pages/logout")?>"><i class="fa fa-arrow-circle-o-left"></i> Logout</a>
                        </li> 
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">            
			<?php $this->load->view($isi); ?>  <!--LOAD PTG-->
		</div>


        <script src="<?php echo base_url("public/template")?>/js/jquery-1.11.0.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/plugins/metisMenu/metisMenu.min.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/plugins/dataTables/jquery.dataTables.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/sb-admin-2.js"></script>
	    <script>
	    $(document).ready(function() {
	        $('#dataTables-example').dataTable();
	    });
	    </script>

    </div>
</body>

</html>
