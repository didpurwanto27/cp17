<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Konfirmasi Pembayaran</title>
    <link href="<?php echo base_url("public/template")?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<?php echo base_url('public')?>/validation/gen_validatorv4.js" type="text/javascript" xml:space="preserve">
    </script>
    <style>
    #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
    }

    #myImg:hover {opacity: 0.7;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }

    /* Modal Content (image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
    }

    /* Caption of Modal Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation */
    .modal-content, #caption {    
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @-webkit-keyframes zoom {
        from {-webkit-transform:scale(0)} 
        to {-webkit-transform:scale(1)}
    }

    @keyframes zoom {
        from {transform:scale(0)} 
        to {transform:scale(1)}
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }
    }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading" align="center">
                        <h3> PENGISIAN DATA SUKSES !!!!</b>
                        </h3>
                        <h5><b>Cek Data Anda, Pastikan Sudah Benar Semua. Jika Ada yang salah, silahkan konfirmasi ulang dan beri pesan di kolom "Pesan Kepada Bendahara"</b>
                        </h5>
                    </div>
                    <div class="panel-body" align="left">
                    
					<?php $this->load->view($isi); ?>  <!--LOAD PTG-->
					
					
					</div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url("public/")?>js/jquery-1.11.0.js"></script>
    <script src="<?php echo base_url("public/")?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/sb-admin-2.js"></script>
</body>
</html>

