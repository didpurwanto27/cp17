<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Konfirmasi Pembayaran</title>
    <link href="<?php echo base_url("public/template")?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<?php echo base_url('public')?>/validation/gen_validatorv4.js" type="text/javascript" xml:space="preserve">
    </script>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	$( function() {
	$( "#datepicker" ).datepicker();
	} );
	</script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading" align="center">
                        <h3> Konfirmasi Pembayaran UT Taiwan </b>
                        </h3>
                        <h4><b>Petunjuk Teknis</b>
                        </h4>
                    </div>
                    <div class="panel-body" align="left">
                        <ul>
                            <li> Mohon mengisi data diri dengan baik dan benar, dilarang mengisi dengan data ambigu <b> (tidak alay)</b>
                            </li>
                            <li> Pastikan proses pengisian form konfirmasi telah selesai. Anda akan mendapatkan halaman 'SUKSES' jika isian Anda sudah selesai.
                            </li>
							<li> Foto yang diupload adalah foto bukti pembayaran (contoh: kwitansi ATM, kantor pos, dll). Bukan foto Selfie atau foto pribadi Anda
                            </li>
                            <li> Jika ada yang ditanyakan bisa menghubungi line bendahara di <p class="btn btn-success" >@tvt3764t <p>
                            </li>
							<li> Pertanyaan terkait modul dapat ditanyakan melalui chat FB UT Taiwan
                            </li>
							
                        </ul>
                        
                    
					<form role="form" action="<?php echo base_url("index.php/pembayaran/insert_data_konfirmasi")?>" method="POST" enctype="multipart/form-data" name="myform" id="myform">
						
					<div class="alert alert-success" >
						<b> LANGKAH 1/3 (Data Mahasiswa)</b> 
						<hr />
						
						<div class="form-group">
							<label>NIM (untuk mahasiswa lama) atau ID (untuk mahasiswa baru)*</label>
							<font color = "red"><div id='myform_nim_errorloc' class="error_strings"></div></font>
							<input class="form-control" name="nim" placeholder="contoh: 018883353">
						</div>
						<div class="form-group">
							<label>NAMA Lengkap *</label>
							<font color = "red"><div id='myform_nama_errorloc' class="error_strings"></div></font>
							<input class="form-control" name="nama" placeholder="contoh: Cintya Navila">
						</div>
						<div class="form-group">
							<label>Jurusan *</label>
							<font color = "red"><div id='myform_jurusan_errorloc' class="error_strings"></div></font>
							<select class="form-control" name="jurusan">                                
								<option selected disabled></option>
								<option value="Manajemen">Manajemen</option>
								<option value="Bahasa Inggris">Bahasa Inggris</option>
								<option value="Ilmu Komunikasi">Ilmu Komunikasi</option>
							</select>    
						</div>
						<div class="form-group">
							<label>Angkatan *</label>
							<font color = "red"><div id='myform_semester_errorloc' class="error_strings"></div></font>
							<select class="form-control" name="semester">                                
								<option selected disabled></option>
								<option value='[2012.1]Angkatan 2'>[2012.1] Angkatan 2</option>
								<option value='[2012.2]Angkatan 3'>[2012.2] Angkatan 3</option>
								<option value='[2013.1]Angkatan 4'>[2013.1] Angkatan 4</option>
								 <option value='[2013.2]Angkatan 5'>[2013.2] Angkatan 5</option>
								<option value='[2014.1]Angkatan 6'>[2014.1] Angkatan 6</option>
								 <option value='[2014.2]Angkatan 7'>[2014.2] Angkatan 7</option>
								 <option value='[2015.1]Angkatan 8'>[2015.1] Angkatan 8</option>
								 <option value='[2016.1]Angkatan 9'>[2016.1] Angkatan 9</option>
								 <option value='[2016.2]Angkatan 10'>[2016.2] Angkatan 10</option>
								 <option value='[2017.1]Angkatan 11'>[2017.1] Angkatan 11</option>
								 <option value='[2018.1]Angkatan 12'>[2018.1] Angkatan 12</option>
								 <option value='[2019.1]Angkatan 13'>[2019.1] Angkatan 13</option>
							 </select>  
							<!--?php	
								echo "<select class=\"form-control\" name=\"semester\">";	
								echo "<option selected disabled></option>";
								for($i=1 ; $i<=13 ; $i++){
									echo "<option>Angkatan $i</option>";									
								}							
								echo "</select>";
							?-->
						</div>
					</div>
						
						
					<div class="alert alert-info" >
					<b> LANGKAH 2/3 (Unggah Bukti Pembayaran)</b> 
						<hr />
						<div class="form-group">	
							<label>Metode Pembayaran *</label>								
							<font color = "red"><div id='myform_metode_errorloc' class="error_strings"></div></font>
							<select class="form-control" name="metode">											
								<option selected disabled></option>
								<option value='NTD'>NTD</option>
								<option value='Rupiah'>Rupiah</option>										
							</select>																
						</div>
						<div class="form-group">
							<label>Total Jumlah Pembayaran *</label>
							<font color = "red"><div id='myform_jumlah_errorloc' class="error_strings"></div></font>
							<input class="form-control" name="jumlah" placeholder="contoh: 7500">
						</div>
						<div class="form-group">
							<label>Atas Nama Pembayar *</label>
							<font color = "red"><div id='myform_atasnama_errorloc' class="error_strings"></div></font>
							<input class="form-control" name="atasnama" placeholder="contoh: Cintya Navila">
						</div>
						<div class="form-group">
							<label>Tanggal Transfer *</label>
							<font color = "red"><div id='myform_tgltransfer_errorloc' class="error_strings"></div></font>
							<!--input class="form-control" name="tgltransfer"-->
							<input type="text" id="datepicker" name="tgltransfer">
						</div>
						<div class="form-group">
							<label>Unggah Bukti Pembayaran *</label>
							<label><i><font size="1px" color="brown">File harus berekstensi .jpg / .png</font></i></label>
							<input type="file" name="userfile">
						</div>
					</div>
					
					<div class="alert alert-warning" >
					<b> LANGKAH 3/3 (Detail Pembayaran)</b> 
						<hr />
						
						
							<label>Centang poin-poin pembayaran Anda.</label><br />
							<input type="checkbox" name="uk" value="1" /> Uang Kuliah (UK) 4.500 NTD/1.750.000 Rupiah<br />
							<input type="checkbox" name="uo" value="1" /> Uang Operasional (UO) 2000 NTD/ 946.000 Rupiah<br />
							<input type="checkbox" name="maba" value="1" /> Uang Pendaftaran Maba   1.500 NTD / 727.500,00 Rupiah<br />
							<br />
							<b>Ujian Ulang (UJUL)</b> <br >
							Jumlah SKS untuk ujian ulang 300 NTD/sks atau 120.000 Rupiah/sks<br />
							Pilih <font color = "red">'TIDAK MENGAMBIL UJUL'</font> jika tidak mengambil UJUL.<br />

							<font color = "red"><div id='myform_sksujul_errorloc' class="error_strings"></div></font>	
							<div class="form-group">
								<?php	
									echo "<select class=\"form-control\" name=\"sksujul\">";	
									echo "<option selected disabled></option>";
									echo "<option> Tidak Mengambil UJUL</option>";									
									for($i=2 ; $i<=24 ; $i++){
										echo "<option> $i SKS</option>";									
									}							
									echo "</select>";
								?>
							</div>
							
							<b> CUTI </b><br /> Pilih <font color = "red">'TIDAK MENGAMBIL CUTI'</font> jika tidak mengambil cuti.<br />
							<font color = "red"><div id='myform_semestercuti_errorloc' class="error_strings"></div></font>	
							<div class="form-group">
								<?php	
									echo "<select class=\"form-control\" name=\"semestercuti\">";	
									echo "<option selected disabled></option>";
									for($i=0 ; $i<=8 ; $i++){
										if($i==0)
											echo "<option>Tidak mengambil cuti</option>";
										else
											echo "<option>Cuti pada semester $i</option>";									
									}							
									echo "</select>";
								?>
							</div>
							<br />
							<label>Pemesanan MODUL. Anda bisa memilih lebih dari satu semester.</label><br />
							<input type="checkbox" name="modul_sem1" value="1" /> MODUL semester 1<br />
							<input type="checkbox" name="modul_sem2" value="1" /> MODUL semester 2<br />
							<input type="checkbox" name="modul_sem3" value="1" /> MODUL semester 3<br />
							<input type="checkbox" name="modul_sem4" value="1" /> MODUL semester 4<br />
							<input type="checkbox" name="modul_sem5" value="1" /> MODUL semester 5<br />
							<input type="checkbox" name="modul_sem6" value="1" /> MODUL semester 6<br />
							<input type="checkbox" name="modul_sem7" value="1" /> MODUL semester 7<br />
							<input type="checkbox" name="modul_sem8" value="1" /> MODUL semester 8<br />
						
						<br />
						<div class="form-group">
							<label>Pesan kepada bendahara</label>
							<textarea class="form-control" rows="5" name="pesan" placeholder="contoh: Konfirmasi pembayaran ini yang kedua kali."></textarea>
						</div>	
						
					<button type="submit" class="btn btn-danger">Konfirmasi Pembayaran</button>		
					</div>					
					<div class="clearfix"></div>
					</form>
					<br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url("public/")?>js/jquery-1.11.0.js"></script>
    <script src="<?php echo base_url("public/")?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/sb-admin-2.js"></script>
	<!-- VALIDATION SCRIPT-->
	<script language="JavaScript" type="text/javascript"
			xml:space="preserve">//<![CDATA[
		//You should create the validator only after the definition of the HTML form
	  	    var frmvalidator  = new Validator("myform");
			frmvalidator.EnableOnPageErrorDisplay();
			frmvalidator.EnableMsgsTogether();

			frmvalidator.addValidation("nim","req","ERROR: Kolom NIM harus diisi!");
			frmvalidator.addValidation("nama","req","ERROR: Kolom NAMA harus diisi!");
			frmvalidator.addValidation("jurusan","req","ERROR: Kolom JURUSAN harus diisi!");
			frmvalidator.addValidation("semester","req","ERROR: Kolom SEMESTER harus diisi!");
			frmvalidator.addValidation("metode","req","ERROR: Kolom METODE PEMBAYARAN harus diisi!");
			frmvalidator.addValidation("jumlah","req","ERROR: Kolom JUMLAH PEMBAYARAN harus diisi!");
			frmvalidator.addValidation("jumlah","num","ERROR: Kolom JUMLAH PEMBAYARAN harus berupa ANGKA!");
			frmvalidator.addValidation("atasnama","req","ERROR: Kolom ATAS NAMA PEMBAYARAN harus diisi!");
			frmvalidator.addValidation("tgltransfer","req","ERROR: Kolom TANGGAL TRANSFER harus diisi!");
			frmvalidator.addValidation("sksujul","req","ERROR: Kolom Ujian Ulang harus diisi!");
			frmvalidator.addValidation("semestercuti","req","ERROR: Kolom Cuti  harus diisi!");
			
			
		//]]>
	</script>
</body>
</html>

