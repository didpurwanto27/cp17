<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Konfirmasi Pembayaran</title>
    <link href="<?php echo base_url("public/template")?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<?php echo base_url('public')?>/validation/gen_validatorv4.js" type="text/javascript" xml:space="preserve">
    </script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading" align="center">
                        <h3> Konfirmasi Pembayaran UT Taiwan <b> 2019.1 </b>
                        </h3>
                        <p>
                        REGISTRASI ONLINE Mahasiswa Baru: 3 Desember 2018 - 1 Januari 2019 <br />
						REGISTRASI ONLINE Mahasiswa Lama : 3 Desember 2018 – 21 Januari 2019 <br />
						REGISTRASI ONLINE pengajuan Ujian Ulang : 3 Desember 2018 – 13 Februari 2019  <br />
						REGISTRASI ON THE SPOT : 23 dan 30 Desember 2018 pukul 10.00-16.00  <br />
						</p>
                    </div>
                    <div class="panel-body" align="center">
                        <h4><b> Petunjuk Lengkap </b></h4>
                        <a href="http://ut-taiwan.org/registrasilama/" class="btn btn-warning">Daftar Ulang</a>
                        <a href="http://ut-taiwan.org/webutt2/index.php/pages/cara_bayar" class="btn btn-info">Cara Bayar</a>
                        <a href="http://ut-taiwan.org/webutt2/index.php/pages/regis_mala" class="btn btn-success">Mahasiswa Lama</a>
                        <hr />
                        <h4><b> Petunjuk Singkat </b></h4>
                    </div>
                        
                    <div class="panel-body" align="left">
                        <ul>
                            <li> Khusus untuk mahasiswa lama, sebelum melakukan konfirmasi pembayaran, Anda harus melakukan registrasi ulang di link <a target="_blank" href="http://ut-taiwan.org/registrasilama/" class="btn btn-danger">Daftar Ulang</a>
                            </li>
                            <li> Jika sudah, maka silahkan melanjutkan ke halaman konfirmasi pembayaran  di link <a target="_blank" href="<?php echo base_url('index.php/pembayaran/konf_page')?>" class="btn btn-danger">Konfirmasi</a>
                            </li>
                            <li> Konfirmasi ulang ke Line bendahara <p class="btn btn-success" >@tvt3764t <p>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url("public/")?>js/jquery-1.11.0.js"></script>
    <script src="<?php echo base_url("public/")?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/sb-admin-2.js"></script>
</body>
</html>

