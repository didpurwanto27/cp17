<div class="table-responsive">
    <table class="table table-hover tablesorter">
        <thead>
            <tr>
                <th class="header">First Name</th>
                <th class="header">Last Name</th>    
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($dataku) && !empty($dataku)) {
                foreach ($dataku as $key => $element) {
                    ?>
                    <tr>
                        <td><?php echo $element['ID_MHS']; ?></td>   
                        <td><?php echo $element['NAMA_MHS']; ?></td>  
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">There is no employee.</td>    
                </tr>
            <?php } ?>
 
        </tbody>
    </table>
    <a class="pull-right btn btn-primary btn-xs" href="<?php echo base_url()?>index.php/pages/createXLS"><i class="fa fa-file-excel-o"></i> Export Data</a>
</div> 