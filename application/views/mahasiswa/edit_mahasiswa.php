<div class="alert alert-default" >					
	<div style="overflow-x:auto;">
	</div>

	<form action="" method="post">
					<fieldset class="registrasi">
						<legend>Informasi Registrasi</legend>
						<div>
							<label>NIM</label>
							<input type="text" name="nim" value="<?php echo $result['NIM_MHS'];?>">
						</div>
						<div>
							<label>Nama Lengkap</label>
							<input type="text" name="nama" value="<?php echo $result['Name_MHS'];?>">
						</div>
						<div>
							<label>Alamat</label>
							<input type="text" name="alamat" value="<?php echo $result['ALAMAT_MHS'];?>">
						</div>
						<div>
							<label>District</label>
							<input type="text" name="district" value="<?php echo $result['DISTRICT_MHS'];?>">
						</div>
						<div>
							<label>Kota</label>
							<input type="text" name="kabkot" value="<?php echo $result['KABKOT_MHS'];?>">
						</div>
						<div>
							<label>Kodepos</label>
							<input type="text" name="kodepos" value="<?php echo $result['KODEPOS_MHS'];?>">
						</div>
						<div>
							<label>No Telepon</label>
							<input type="text" name="telp" value="<?php echo $result['TELP_MHS'];?>">
						</div>
						<div>
							<label>Handphone</label>
							<input type="text" name="hp" value="<?php echo $result['CELLPHONE_MHS'];?>">
						</div>
						<div>
							<label>Email</label>
							<input type="text" name="email" value="<?php echo $result['EMAIL_MHS'];?>">
						</div>
						<div>
							<label>Tanggal Lahir</label>
							<input type="text" id="datepicker" name="tgllahir" value="<?php echo $result['TGL_LHR_MHS'];?>" class="input100 inputdate hasDatepicker">
						</div>
						<div>
							<label>Tempat Lahir</label>
							<input type="text" name="tmplahir" value="<?php echo $result['TMP_LHR_MHS'];?>">
						</div>
						<div>
							<label>Agama</label>
							<select name="agama">
								<option value="islam">Islam</option>
								<option value="katolik">Katolik</option>
								<option value="protestan">Protestan</option>
								<option value="hindu">Hindu</option>
								<option value="budha">Budha</option>								
							</select>							
						</div>
						<div>
							<label>Program Studi</label>
							<select name="prodi">
								<option value="54">Manajemen</option>
								<option value="72">Ilmu Komunikasi</option>
								<option value="87">Bahasa Inggris (Penerjemah)</option>								
							</select>
						</div>
						<div>
							<label>Kode UPBJJ</label>
							<input type="text" name="upbjj" value="<?php echo $result['KBUPBJJ_MHS'];?>" disabled="disable">
						</div>						
						<div>
							<label>Jenis Kelamin</label>
							<select name="jk">
								<option value="Pria">Pria</option>
								<option value="Wanita">Wanita</option>						
							</select>
						</div>
						<div>
							<label>Kewarganegaraan</label>
							<select name="warganegara">
								<option value="WNI">WNI</option>
								<option value="Asing">Asing</option>						
							</select>
						</div>
						<div>
							<label>Pekerjaan</label>
							<select name="pekerjaan">
								<option value="Swasta / TKI">Swasta / TKI</option>
								<option value="TNI/Polri">TNI/Polri</option>
								<option value="Wiraswasta">Wiraswasta</option>
								<option value="Tidak Bekerja">Tidak Bekerja</option>
								<option value="PNS">PNS</option>						
							</select>
						</div>
						<div>
							<label>Status Pernikahan</label>
							<select name="kawin">
								<option value="Menikah">Menikah</option>
								<option value="Tidak Menikah">Tidak Menikah</option>						
							</select>
						</div>
                        <div>
							<label>Tempat Ujian</label>
							<select name="tempatujian">
								<option value="1">Taipei</option>
								<option value="2">Tainan</option>
                                <option value="3">Indonesia</option>						
							</select>
						</div>
					</fieldset>
					<fieldset class="pendidikan">
						<legend>Pendidikan Terakhir</legend>
						<div>
							<label>Jenjang</label>
							<select name="jenjang">
								<option value="SLTA">SLTA</option>
								<option value="D-I">D-I</option>	
								<option value="D-II">D-II</option>
								<option value="D-III">D-III</option>		
							</select>
						</div>
						<div>
							<label>Jurusan</label>
							<select name="jurusan">
								<option value="SMTA Umum IPA / IPS">SMTA Umum IPA / IPS</option>
								<option value="STM/SMTA">STM/SMTA</option>	
								<option value="SMK/SMKA">SMK/SMKA</option>
								<option value="SMEA">SMEA</option>		
								<option value="SPMA">SPMA</option>		
								<option value="SPG/SPGLB/SGO/SMOA">SPG/SPGLB/SGO/SMOA</option>	
							</select>
						</div>
						<div>
							<label>Tahun Ijazah</label>
							<input type="text" name="thnijazah" value="<?php echo $result['THNIJAZAH_MHS'];?>">
						</div>
						<div>
							<label>Nama Ibu</label>
							<input type="text" name="ibu" value="<?php echo $result['NAMAIBU_MHS'];?>">
						</div>
						<div>
							<label>No. Rekening</label>
							<input type="text" name="rekening" value="<?php echo $result['NOREKENING_MHS'];?>">
						</div>
						<div>
							<label>Nama Bank</label>
							<input type="text" name="bank" value="<?php echo $result['BANK_MHS'];?>">
						</div>
					</fieldset>
					<fieldset>
						<legend>Informasi Lainnya</legend>
						<div>
							<label>Nama Facebook</label>
							<input type="text" name="fb" value="<?php echo $result['NAMAFB_MHS'];?>">
						</div>
						<div>
							<label>Angkatan</label>
							<input type="text" name="angkatan" value="<?php echo $result['ANGKATAN_MHS'];?>">
						</div>
						<div>
							<label>Semester Terakhir</label>
							<input type="text" name="semester" value="<?php echo $result['SEMESTER_AKTIF'];?>">
						</div>
						<div>
							<label><b>Status Mahasiswa</b></label>
							<select name="status">
								<option value="0">Belum Update</option>
								<option value="1">Aktif</option>	
								<option value="2">Cuti</option>
								<option value="3">Berhenti/ke Indonesia</option>	
							</select>
						</div>
					</fieldset>
          <div>
               <button type="submit" name="submit" class="edit">Submit</button>
          </div>
				</form>

	<br />
	<br />
	<div class="clearfix"></div>

<br />
</div>



<!-- The Modal -->
<!-- <div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script> -->