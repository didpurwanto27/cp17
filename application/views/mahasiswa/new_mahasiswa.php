 <div class="alert alert-default" >					
	<div style="overflow-x:auto;">
	</div>

	<form role="form" action="<?php echo base_url("index.php/mahasiswa/do_add_mahasiswa")?>" method="POST" enctype="multipart/form-data" name="add_mahasiswa_form" id="myform">
		<table>
			<tr>
				<td width="200px">NIM
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="nim">
				</td>
			</tr>
			<tr>
				<td width="200px">Nama Lengkap
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="nama">
				</td>
			</tr>
			<tr>
				<td width="200px">Alamat Mahasiswa
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="alamat">
				</td>
			</tr>
			<tr>
				<td width="200px">District
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="district">
				</td>
			</tr>
			<tr>
				<td width="200px">Kota
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="kabkot">
				</td>
			</tr>
			<tr>
				<td width="200px">Kode Pos
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="kode_pos">
				</td>
			</tr>
			<tr>
				<td width="200px">No Telepon
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="telp">
				</td>
			</tr>
			<tr>
				<td width="200px">Handphone
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="hp">
				</td>
			</tr>
			<tr>
				<td width="200px">Email
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="email">
				</td>
			</tr>
			<tr>
				<td width="200px">Tempat Lahir
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="tmplahir">
				</td>
			</tr>
			<tr>
				<td width="200px">Tanggal Lahir
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" id="datepicker" name="tgllahir" class="input100 inputdate hasDatepicker">
				</td>
			</tr>
			<tr>
				<td width="200px">Agama
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="agama">
						<option value="islam">Islam</option>
						<option value="katolik">Katolik</option>
						<option value="protestan">Protestan</option>
						<option value="hindu">Hindu</option>
						<option value="budha">Budha</option>								
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Program Studi
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="prodi">
						<option value="54">Manajemen</option>
						<option value="72">Ilmu Komunikasi</option>
						<option value="87">Bahasa Inggris (Penerjemah)</option>								
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Kode UPBJJ
				</td>
				<td width="10px">:
				</td>
				<td><input type="text" name="upbjj">
				</td>
			</tr>
			<tr>
				<td width="200px">Jenis Kelamin
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="jk">
						<option value="Pria">Pria</option>
						<option value="Wanita">Wanita</option>						
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Kewarganegaraan
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="warganegara">
						<option value="WNI">WNI</option>
						<option value="Asing">Asing</option>						
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Pekerjaan
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="pekerjaan">
						<option value="Swasta / TKI">Swasta / TKI</option>
						<option value="TNI/Polri">TNI/Polri</option>
						<option value="Wiraswasta">Wiraswasta</option>
						<option value="Tidak Bekerja">Tidak Bekerja</option>
						<option value="PNS">PNS</option>						
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Status Pernikahan
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="kawin">
						<option value="Menikah">Menikah</option>
						<option value="Tidak Menikah">Tidak Menikah</option>						
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Tempat Ujian
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="tempatujian">
						<option value="1">Taipei</option>
						<option value="2">Tainan</option>
                        <option value="3">Indonesia</option>						
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Pendidikan Terakhir
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="jenjang">
						<option value="SLTA">SLTA</option>
						<option value="D-I">D-I</option>	
						<option value="D-II">D-II</option>
						<option value="D-III">D-III</option>						
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Jurusan
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="jurusan">
						<option value="SMTA Umum IPA / IPS">SMTA Umum IPA / IPS</option>
						<option value="STM/SMTA">STM/SMTA</option>	
						<option value="SMK/SMKA">SMK/SMKA</option>
						<option value="SMEA">SMEA</option>		
						<option value="SPMA">SPMA</option>		
						<option value="SPG/SPGLB/SGO/SMOA">SPG/SPGLB/SGO/SMOA</option>	
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">Tahun Ijazah
				</td>
				<td width="10px">:
				</td>
				<td>
					<input type="text" name="thnijazah">
				</td>
			</tr>
			<tr>
				<td width="200px">Nama Ibu
				</td>
				<td width="10px">:
				</td>
				<td>
					<input type="text" name="nama_ibu">
				</td>
			</tr>
			<tr>
				<td width="200px">No. Rekening
				</td>
				<td width="10px">:
				</td>
				<td>
					<input type="text" name="rekening">
				</td>
			</tr>
			<tr>
				<td width="200px">Nama Bank
				</td>
				<td width="10px">:
				</td>
				<td>
					<input type="text" name="bank">
				</td>
			</tr>
			<tr>
				<td width="200px">Informasi Lainnya
				</td>
			</tr>
			<tr>
				<td width="200px">Nama Facebook
				</td>
				<td width="10px">:
				</td>
				<td>
					<input type="text" name="fb">
				</td>
			</tr>
			<tr>
				<td width="200px">Angkatan
				</td>
				<td width="10px">:
				</td>
				<td>
					<input type="text" name="angkatan">
				</td>
			</tr>
			<tr>
				<td width="200px">Semester Terakhir
				</td>
				<td width="10px">:
				</td>
				<td>
					<input type="text" name="semester">
				</td>
			</tr>
			<tr>
				<td width="200px">Status Mahasiswa
				</td>
				<td width="10px">:
				</td>
				<td>
					<select name="status">
						<option value="0">Belum Update</option>
						<option value="1">Aktif</option>	
						<option value="2">Cuti</option>
						<option value="3">Berhenti/ke Indonesia</option>	
					</select>
				</td>
			</tr>
		</table>	
		</br>
		<input class="btn btn-primary" type="submit" name="submit" value="Add New Mahasiswa" />
	</form>

<br />
</div>



<!-- The Modal -->
<!-- <div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script> -->