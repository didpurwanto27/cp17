<br />
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
        	<div class = "col-lg-12"><h1>Mahasiswa </h1></div>
        	<div class="panel-heading">
        		<a class="btn btn-success" href="<?php echo base_url('index.php/mahasiswa/add_mahasiswa')?>"> Add Mahasiswa</a>
            </div>
            <!-- Form for filter -->
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
								<th>No</th>
                                <th>ID</th>
                                <th>NIM</th>                                            
								<th>Nama</th> 
								<th>Email</th>          			
								<th>HP</th> 
								<th>Registrasi</th> 
								<th>Agama</th> 
								<th>Ujian</th> 
								<th>Operation</th> 
                            </tr>
                        </thead>
						<!-- show data table -->
						<?php
								$i=1;
								foreach($result as $data)
								{
									// print_r($data);
									echo "<tr class=\"odd gradeX\" align=\"center\">";
									echo "<td>".$i."</td>";
										$i = $i+1;
									echo "<td align='left'>".$data['ID_MHS']."</td>";
									echo "<td align='left'>".$data['NIM_MHS']."</td>";
									echo "<td align='left'>".$data['NAMA_MHS']."</td>";
									echo "<td align='left'>".$data['EMAIL_MHS']."</td>";
									echo "<td align='left'>".$data['CELLPHONE_MHS']."</td>";
									echo "<td align='left'>".$data['SEMESTER_AKTIF']."</td>";
									echo "<td align='left'>".$data['AGAMA_MHS']."</td>";
									echo "<td align='left'>".$data['TEMPATUJIAN']."</td>";
									// echo "<form role=\"form\" action=\"".base_url('index.php/pages/detail_ebook')."/".$data['ID_EBOOK']."\"> ";							
									echo "<td><a class=\"btn btn-primary\" href=\"".base_url('index.php/mahasiswa/detail_mahasiswa')."/".$data['ID_MHS']."\"> <font color=\"white\">Detail</font></a></td>";
									// echo "<td><a href=\"".base_url('index.php/pages/detail_ebook_guest')."/".$data['ID_EBOOK']."\"> <font color=\"blue\">Detail</font></a></td>";
									// echo "</form>";												
								}
						?>							
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>