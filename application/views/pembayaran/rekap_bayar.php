
<br />
<br />
<br />
<div class="col-lg-7">
	<div class="alert alert-success"><h2>Download Rekap Pembayaran</h2> <br />
	</div>

	<form role="form" action="<?php echo base_url("index.php/pembayaran/download_rekapbayar")?>" method="POST" enctype="multipart/form-data" name="myform" id="myform">
	<div class="form-group">
		<label>Periode</label>
		<select class="form-control" name="semester">                                
			<option selected disabled></option>
			<option value='[2012.1]Angkatan 2'>[2012.1] Angkatan 2</option>
			<option value='[2012.2]Angkatan 3'>[2012.2] Angkatan 3</option>
			<option value='[2013.1]Angkatan 4'>[2013.1] Angkatan 4</option>
			 <option value='[2013.2]Angkatan 5'>[2013.2] Angkatan 5</option>
			<option value='[2014.1]Angkatan 6'>[2014.1] Angkatan 6</option>
			 <option value='[2014.2]Angkatan 7'>[2014.2] Angkatan 7</option>
			 <option value='[2015.1]Angkatan 8'>[2015.1] Angkatan 8</option>
			 <option value='[2016.1]Angkatan 9'>[2016.1] Angkatan 9</option>
			 <option value='[2016.2]Angkatan 10'>[2016.2] Angkatan 10</option>
			 <option value='[2017.1]Angkatan 11'>[2017.1] Angkatan 11</option>
			 <option value='[2018.1]Angkatan 12'>[2018.1] Angkatan 12</option>
			 <option value='[2019.1]Angkatan 13'>[2019.1] Angkatan 13</option>
		 </select>  
		
	</div>
	<button type="submit" class="btn btn-primary">Download</button>		
	</form>
</div>