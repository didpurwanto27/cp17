<div class="alert alert-default" >					
	<div style="overflow-x:auto;">

	
	<a class="btn btn-primary" href="<?php echo base_url('index.php/pembayaran/homebayar')?> ">Kembali</a>
	<hr />

	  <table class="table">
		<tr>
			<td width="200px">Status 
			</td>
			<td width="10px">:
			</td>
			<td><?php 
			if($status==1)
				echo "<font color=\"green\"><b>Sukses</b></font>";
			else
				echo "<font color=\"red\"><b>Proses</b></font>";
			?>
			</td>
		<tr>

			<tr>
			<td width="200px">Bayar ID
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $bayar_id?>
			</td>
		<tr>
		<tr>
			<td width="200px">Semester Bayar
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $semester_bayar?>
			</td>
		<tr>

		<tr>
			<td width="200px">Nama
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $nama?>
			</td>
		<tr>
		<tr>
			<td width="200px">NIM
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $nim?>
			</td>
		<tr>
		<tr>
			<td width="200px">Jurusan
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $jurusan?>
			</td>
		<tr>
		<tr>
			<td width="200px">Semester
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $semester?>
			</td>
		<tr>
		<tr>
			<td width="200px">Tanggal Bayar
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $tgltransfer?>
			</td>
		<tr>

		<tr>
			<td width="200px">Metode Pembayaran
			</td>
			<td width="10px">:
			</td>
			<td><b><?php echo $metode?></b>
			</td>
		<tr>
		
		<tr>
			<td width="200px">Jumlah Pembayaran
			</td>
			<td width="10px">:
			</td>
			<td><b><?php echo $jumlah?></b>
			</td>
		<tr>
		<tr>
			<td width="200px">Atas Nama
			</td>
			<td width="10px">:
			</td>
			<td><b><?php echo $atasnama?></b>
			</td>
		<tr>
		<tr>
			<td width="200px">Bukti Pembayaran
			</td>
			<td width="10px">:
			</td>
			<?php
			$folder='data_pembayaran/';
			$urlimage=$folder .'' .$path_image;
			//echo $urlimage
			?>
			<td><img id="myImg" src="<?php echo base_url($urlimage)?>" alt="Bukti Pembayaran" style="width:100%;max-width:300px">
			</td>
		<tr>
		<tr>
			<td width="200px"><strong>Detail Pembayaran</strong>
			</td>
			<td width="10px">
			</td>
			<td>
			</td>
		<tr>
		<tr>
			<td width="200px">UK
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($uk==1)
				echo "<font color=\"green\"><b>Iya</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">UO
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($uo==1)
				echo "<font color=\"green\"><b>Iya</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Maba
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($maba==1)
				echo "<font color=\"green\"><b>Iya</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Ujian Ulang (UJUL)
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $sksujul?>
			</td>
		<tr>
		<tr>
			<td width="200px">Semester Cuti
			</td>
			<td width="10px">:
			</td>
			<td><?php echo $semestercuti?>
			</td>
		<tr>
		
		<tr>
			<td width="200px"><b>Pemesanan Modul</b>
			</td>
			<td width="10px">&nbsp
			</td>
			<td>
			&nbsp
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 1
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem1==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 2
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem2==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 3
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem3==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 4
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem4==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 5
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem5==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 6
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem6==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 7
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem7==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 8
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php 
			if($modul_sem8==1)
				echo "<font color=\"green\"><b>Pesan</b></font>";
			else
				echo "<font color=\"red\"><b>Tidak</b></font>";
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Pesan kepada bendahara
			</td>
			<td width="10px">:
			</td>
			<td><textarea disabled rows="4" cols="50"><?php echo $pesan?></textarea>
			</td>
		<tr>
	  </table>
	</div>

	<br />
	<br />

	
	<?php 
	echo "<td><a class=\"btn btn-success\" href=\"".base_url('index.php/pembayaran/konfirmasiBayar')."/".$bayar_id."\"> <font color=\"white\">Konfirmasi</font></a></td>";
	echo "&nbsp";

	echo "<td><a class=\"btn btn-primary\" href=\"".base_url('index.php/pembayaran/ubahBayar')."/".$bayar_id."\"> <font color=\"white\">Edit </font></a></td>";
	echo "&nbsp";

	echo "<td><a class=\"btn btn-danger\" href=\"".base_url('index.php/pembayaran/hapus_data')."/".$bayar_id."\"> <font color=\"white\">Hapus</font></a></td>";
	?>
	
	
	<div class="clearfix"></div>

<br />
</div>



<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>