<div class="alert alert-default" >					
	<div style="overflow-x:auto;">

	<form role="form" action="<?php echo base_url("index.php/pembayaran/ubahDataBayar")?>" method="POST" enctype="multipart/form-data" name="myform" id="myform">

	  <table class="table">
		
		<tr>
			<td width="200px">Bayar ID
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="bayar_id" readonly="true" value="<?php echo $bayar_id?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Semester Bayar
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="semester_bayar" readonly="true" value="<?php echo $semester_bayar?>">
			</td>
		<tr>

		<tr>
			<td width="200px">Nama
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="nama" readonly="true" value="<?php echo $nama?>">
			</td>
		<tr>
		<tr>
			<td width="200px">NIM
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="nim" readonly="true" value="<?php echo $nim?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Semester
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="semester" readonly="true" value="<?php echo $semester?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Tanggal Bayar
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="tgltransfer" readonly="true" value="<?php echo $tgltransfer?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Metode Pembayaran
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="metode" value="<?php echo $metode?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Jumlah Pembayaran
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="jumlah" value="<?php echo $jumlah?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Atas Nama
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="atasnama" value="<?php echo $atasnama?>">
			</td>
		<tr>
		
		
		<tr>
			<td width="200px"><strong>Detail Pembayaran</strong>
			</td>
			<td width="10px">
			</td>
			<td>
			</td>
		<tr>
		<tr>
			<td width="200px">UK
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($uk==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uk\" value=1 checked>Iya</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uk\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uk\" value=1>Iya</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uk\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">UO
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($uo==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uo\" value=1 checked>Iya</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uo\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uo\" value=1>Iya</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"uo\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Maba
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($maba==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"maba\" value=1 checked>Iya</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"maba\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"maba\" value=1>Iya</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"maba\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Ujian Ulang (UJUL) 
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php	
				echo "<select class=\"form-control\" name=\"sksujul\">";
				if($sksujul=="Tidak Mengambil UJUL"){
					echo "<option value=\"Tidak Mengambil UJUL\" selected> Tidak Mengambil UJUL</option>";									
					for($i=2 ; $i<=24 ; $i++){
						echo "<option> $i SKS</option>";									
					}							
					echo "</select>";
				}
				else{
					echo "<option value=\"Tidak Mengambil UJUL\" > Tidak Mengambil UJUL</option>";									
					for($i=2 ; $i<=24 ; $i++){
						if($sksujul=="$i SKS")
							echo "<option value=\"$i SKS\" selected> $i SKS</option>";
						else
							echo "<option value=\"$i SKS\" > $i SKS</option>";
					}							
					echo "</select>";
				}
			?>
			</td>
		<tr>
		<tr>
			<td width="200px">Semester Cuti
			</td>
			<td width="10px">:
			</td>
			<td>
			<?php
				echo "<select class=\"form-control\" name=\"semestercuti\">";
				if($semestercuti=="Tidak mengambil cuti"){
					echo "<option value=\"Tidak mengambil cuti\" selected> Tidak mengambil cuti</option>";									
					for($i=1 ; $i<=8 ; $i++){
						echo "<option value=\"Cuti pada semester $i\">Cuti pada semester $i</option>";									
					}							
					echo "</select>";
				}
				else{
					echo "<option value=\"Tidak mengambil cuti\" > Tidak mengambil cuti</option>";									
					for($i=1 ; $i<=8 ; $i++){
						if($semestercuti=="Cuti pada semester $i")
							echo "<option value=\"Cuti pada semester $i\" selected>Cuti pada semester $i</option>";	
						else
							echo "<option value=\"Cuti pada semester $i\" >Cuti pada semester $i</option>";
					}							
					echo "</select>";
				}
			?>
			</td>
		<tr>
		
		<tr>
			<td width="200px"><b>Pemesanan Modul</b>
			</td>
			<td width="10px">&nbsp
			</td>
			<td>
			&nbsp
			</td>
		<tr>
		<tr>
			<td width="200px">Modul Semester 1
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem1==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem1\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem1\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem1\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem1\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Modul Semester 2
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem2==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem2\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem2\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem2\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem2\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Modul Semester 3
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem3==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem3\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem3\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem3\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem3\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Modul Semester 4
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem4==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem4\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem4\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem4\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem4\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Modul Semester 5
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem5==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem5\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem5\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem5\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem5\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Modul Semester 6
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem6==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem6\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem6\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem6\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem6\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Modul Semester 7
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem7==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem7\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem7\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem7\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem7\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Modul Semester 8
			</td>
			<td width="10px">:
			</td>
			<?php 
			if($modul_sem8==1)
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem8\" value=1 checked>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem8\" value=0>Tidak</input>
						</label>
					</td>";
			else
				echo "<td>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem8\" value=1>Pesan</input>
						</label>
						<label class=\"radio-inline\">
							<input type=\"radio\" name=\"modul_sem8\" value=0 checked>Tidak</input>
						</label>
					</td>";
			?>
		<tr>
		<tr>
			<td width="200px">Pesan kepada bendahara
			</td>
			<td width="10px">:
			</td>
			<td><textarea disabled rows="4" cols="50"><?php echo $pesan?></textarea>
			</td>
		<tr>
	  </table>
	</div>

	<a class="btn btn-primary" href="<?php echo base_url('index.php/pembayaran/homebayar')?> ">Data Pembayaran</a>
	<button type="submit" class="btn btn-success">Update Data</button>		

	</form>
	<div class="clearfix"></div>

<br />
</div>