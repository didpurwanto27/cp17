<br />
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
        	<div class="panel-heading">
                <a href="<?php echo base_url("index.php/pembayaran/homebayar")?>" class="btn btn-success">Refresh</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
								<th>No</th>
                                <th>Periode</th>
                                <th>ID Bayar</th>
                                <th>NIM</th>                                            
								<th>Nama</th> 
                                <th>Jurusan</th> 
                                <th>Angkatan</th>          			
                                <th>Metode</th> 
                                <th>Jumlah</th> 
                                <th>Status</th> 
                                <th>Detail</th> 
                                <th>Detail</th> 
                            </tr>
                        </thead>
						<!-- show data table-->
						<?php
								$i=1;
								foreach($result as $data)
								{
									echo "<tr class=\"odd gradeX\" align=\"center\">";
									echo "<td>".$i."</td>";
										$i = $i+1;
									if($data['semester_bayar']%2==0)
                                        //echo "<td align='left'>".$data['semester_bayar']."</td>";
                                        echo "<td align='left'><font color=\"blue\">".$data['semester_bayar']."</font></td>";
                                    else
                                        echo "<td align='left'><font color=\"brown\">".$data['semester_bayar']."</font></td>";
                                    echo "<td align='left'>".$data['bayar_id']."</td>";
									echo "<td align='left'>".$data['nim']."</td>";
									echo "<td align='left'>".$data['nama']."</td>";
                                    echo "<td align='left'>".$data['jurusan']."</td>";
                                    echo "<td align='left'>".$data['semester']."</td>";
                                    echo "<td align='left'>".$data['metode']."</td>";
                                    echo "<td align='left'>".$data['jumlah']."</td>";
                                    if($data['status']==1)
                                        echo "<td align='left'><font color=\"green\"><b>Sukses</b></font></td>";
                                    else
                                        echo "<td align='left'><font color=\"red\"><b>Proses</b></font></td>";

									/*echo "<form role=\"form\" action=\"".base_url('index.php/pembayaran/detail_bayar')."/".$data['bayar_id']."\"> ";												
									echo "<td><button type=\"submit\" class=\"btn btn-primary\">Proses</button></td>";
									echo "</form>";

                                    echo "<form role=\"form\" action=\"".base_url('index.php/pembayaran/hapus_data')."/".$data['bayar_id']."\"> ";                                              
                                    echo "<td><button type=\"submit\" class=\"btn btn-danger\">Hapus</button></td>";
                                    echo "</form>";*/

                                    echo "<td><a class=\"btn btn-primary\" href=\"".base_url('index.php/pembayaran/detail_bayar')."/".$data['bayar_id']."\"> <font color=\"white\">Proses</font></a></td>";
                                    echo "<td><a class=\"btn btn-danger\" href=\"".base_url('index.php/pembayaran/hapus_data')."/".$data['bayar_id']."\"> <font color=\"white\">Hapus</font></a></td>";

								}
						?>									
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

