<br />
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" align="center">
            <h2>Status Pembayaran UT Taiwan</h2>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Periode</th>
                                <th>ID Bayar</th>
                                <th>NIM</th>                                            
                                <th>Nama</th> 
                                <th>Jurusan</th> 
                                <th>Angkatan</th>                   
                                <th>Metode</th> 
                                <th>Jumlah</th> 
                                <th>Status</th> 
                            </tr>
                        </thead>
                        <!-- show data table-->
                        <?php
                                $i=1;
                                foreach($result as $data)
                                {
                                    echo "<tr class=\"odd gradeX\" align=\"center\">";
                                    echo "<td>".$i."</td>";
                                        $i = $i+1;
                                    echo "<td align='left'>".$data['semester_bayar']."</td>";
                                    echo "<td align='left'>".$data['bayar_id']."</td>";
                                    echo "<td align='left'>".$data['nim']."</td>";
                                    echo "<td align='left'>".$data['nama']."</td>";
                                    echo "<td align='left'>".$data['jurusan']."</td>";
                                    echo "<td align='left'>".$data['semester']."</td>";
                                    echo "<td align='left'>".$data['metode']."</td>";
                                    echo "<td align='left'>".$data['jumlah']."</td>";
                                    if($data['status']==1)
                                        echo "<td align='left'><font color=\"green\"><b>Sukses</b></font></td>";
                                    else
                                        echo "<td align='left'><font color=\"red\"><b>Menunggu</b></font></td>";

                                }
                        ?>                                  
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>