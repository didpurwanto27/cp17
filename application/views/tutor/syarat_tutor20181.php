
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<br />
					<div align="center" >
						<a href="#matkul" class="btn btn-danger">Mata Kuliah  </a>
						<a href="#syarat" class="btn btn-danger">Syarat</a>
						<a href="#untung" class="btn btn-danger">Keuntungan</a>
						<a href="#lamar" class="btn btn-danger">Pengajuan Lamaran</a>
						
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<section id = "matkul">
				<div class="alert alert-info" >
					<div class="post-image">
						<div class="post-heading">
							<h3>Persyaratan Tutor 2018.1</h3>
						</div>
					</div>
					<p>
					Universitas Terbuka-Taiwan membuka kesempatan bagi putra/putri terbaik bangsa Indonesia untuk bergabung sebagai Calon Tenaga Pengajar (Tutor) Formasi Tahun 2018.1, dengan periode pendaftaran sampai tanggal <b> 18 Januari 2018</b>.
					
					<hr />
					</p>
					<p>
					<h4>
					Formasi yang dibuka adalah:
					</h4>
					</p>
					<ol>
						<li>JURUSAN</li>
							<ul>
								<li>Manajemen</li>
								<li>Ilmu Komunikasi</li>
								<li>Bahasa Inggris (penerjemah)</li>
							</ul>
					</ol>
					
					<ol>
						<li>DETAIL MATA KULIAH</li>
							<ul>
								<li>Manajemen</li>
									<ol>
										<li>Pengantar Bisnis</li>
										<li>Manajemen</li>
										<li>Matematika Ekonomi</li>
										<li>Organisasi</li>
										<li>Akuntansi Biaya</li>
										<li>Perilaku Konsumen</li>
										<li>Teori Portofolio dan Analisis Investasi</li>
										<li>Manajemen Kualitas</li>
									</ol>
								<li>Ilmu Komunikasi</li>
									<ol>
										<li>Komunikasi Bisnis</li>
										<li>Hukum Media Massa</li>
										<li>Konsultasi 1 </li>
											<ul>
												 <li>Opini Publik</li>
												 <li>Teknik Mencari dan Menulis Berita</li>
												 <li>Public Speaking</li>
											 </ul>
										<li>Konsultasi 2 </li>
											 <ul>
												<li>Teori Komunikasi</li>
												 <li>Komunikasi Massa</li>
												 <li>Psikologi Komunikasi </li>
											</ul>
										<li>Konsultasi 3 </li>
											 <ul>
												<li>Perencanaan Program Komunikasi </li>
												<li>Komunikasi Organisasi </li>
											</ul>
										<li>Konsultasi 4 </li>
											 <ul>
												<li>Perkembangan Teknologi Komunikasi</li>
												<li>Komunikasi Internasional</li>
												<li>Teknik Hubungan Masyarakat </li>
											</ul>	
							
									</ol>
								
								<li>Bahasa Inggris</li>
									<ol>
										<li>Konsultasi 1 </li>
											<ul>
												<li>Writing IV</li>
												<li>Translation 1</li>
												<li>Reading IV</li>
											 </ul>
										<li>Bahasa Indonesia Tata Bahasa dan Komposisi</li>
										<li>Konsultasi 2 </li>
											 <ul>
												<li>Translation 7</li>
												 <li>Translation 8</li>
												 <li>Translation 9</li>
											</ul>
										<li>Penerjemahan Karya Fiksi</li>
										<li>Morfologi & Sintaksis Bahasa Indonesia</li>
										
									</ol>
							</ul>
							
					</ol>
				</div>
				</section>
				<section id="syarat">
					<div class="alert alert-info" >
						<div class="post-heading">
							<h3>Persyaratan Tutor 2018.1</h3>
						</div>
						<ol>
							<ul>
								<li>Warga Negara Republik Indonesia, yang bertakwa kepada Tuhan Yang Maha Esa, setia dan taat kepada Pancasila, UUD Negara Republik Indonesia Tahun 1945 dan Negara Kesatuan Republik Indonesia, serta Bhinneka Tunggal Ika.</li>
								<li>	Tidak sedang atau pernah terlibat di kegiatan atau kepengurusan partai politik.</li>
								<li>	Memiliki komitmen yang tinggi untuk menjadi Tutor dan melaksanakan proses belajar-mengajar.</li>
								<li>	Mampu atau bersedia belajar mengoperasikan komputer, menggunakan perangkat lunak Office dan Online Distance Learning.</li>
								<li>	Mengikuti seluruh peraturan yang ada di Universitas Terbuka Taiwan</li>
								<li>	Memenuhi salah satu dari kriteria berikut:</li>
								<li>	Pendidikan minimal S2 dengan status guru atau minimal S1 dengan status dosen yang memiliki pengalaman mengajar minimal selama 3 tahun.</li>
								<li>	Praktisi dengan pengalaman minimal selama 5 tahun dan memiliki keahlian yang sesuai dengan mata kuliah yang akan diampu.</li>
								<li>	Pernah mengikuti mata kuliah yang akan diajarkan dengan nilai minimal B.</li>
								<li>	Bersedia mengikuti seluruh peraturan yang ada di Universitas Terbuka khususnya yang berkaitan dengan Tutor (mengikuti pelatihan Tutor, membuat RAT, SAT, absensi, nilai, CPT, dan lain-lain).</li>
								<li>	Bersedia menandatangani kontrak sebagai tutor yang diwakili oleh surat kesediaan menjadi tutor.</li>
								<li>	Bekerja dengan profesional dan bertanggung jawab</li>
								<li>	 Berdomisili di Taiwan dari 4 Februari sampai 6 Mei 2018</li>
							</ul>
						</ol>
					</div>
				</section>
				
				<section id="untung">
					<div class="alert alert-info" >
						<div class="post-heading">
							<h3>Keuntungan</h3>
						</div>
						<ul>
							<li>Pahala dari Tuhan Yang Maha Esa karena berkenan untuk membagi ilmunya</li>
							<li>Honor, sesuai standar tutor Universitas Terbuka-Jakarta (standar yang sama dengan tutor UT Indonesia di seluruh dunia)</li>
							<li>Sertifikat mengajar</li>
							<li>Surat Keputusan (SK)</li>
						</ul>
					
					</div>
				</section>
				
				<section id="lamar">
					<div class="alert alert-info" >
						<div class="post-heading">
							<h3>Pengajuan Lamaran</h3>
							<li>Seluruh berkas berikut diunggah di <a href="<?php echo base_url("index.php/pages/daftar_tutor")?>" class="btn btn-success">portal pendaftaran</a> paling lambat pada tanggal 18 Januari  2018 pukul 21.00 (GMT+08:00)</li>
						</div>
						<ul>
							<li>Mengajukan Surat Kesediaan Tutor Mengajar</li>
							<li>Photocopy Ijazah terakhir yang telah dilegalisir oleh pejabat berwenang terbaru</li>
							<li>Photocopy Transkrip Nilai S1 dan S2 yang telah dilegalisir oleh pejabat berwenang</li>
							<li>Daftar Riwayat hidup</li>
							<li>Pas Photo ukuran 4 x 6</li>
							
							
							<li> Surat Kesediaan Tutor Mengajar <a href="http://bit.ly/FormatSuratKesediaanTutor_2018-1" class="btn btn-primary">  Download</a></li>
						</ul>
						
						<hr>
						Info tambahan:	
						<br />
						<a href="http://ut-taiwan.org/images/2017/deskripsi_2017.zip" class="btn btn-primary">Download Deskripsi Mata Kuliah</a>
						<br />
						
					</div>
				</section>
					
					
					
				</div>
			</div>	
		</div>




