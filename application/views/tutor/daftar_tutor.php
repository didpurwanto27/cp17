<!-- VALIDATION SCRIPT-->
	<script language="JavaScript" src="<?php echo base_url('public')?>/validation/gen_validatorv4.js"
		type="text/javascript" xml:space="preserve">
	</script>

	
	<div class="container" >
		<div class="row" >
		<div  >
			<div class="col-lg-5" >
					<div class="alert alert-info" >
							 <form role="form" action="<?php echo base_url("index.php/pages/insert_tutor")?>" method="POST" enctype="multipart/form-data" name="myform" id="myform">
								<div align="center" >
									<h3>Pendaftaran Tutor UTT Taiwan</h3>
									<h5> <font size="3px" color="brown">Mohon gunakan tulisan latin dalam pengisian data (bukan tulisan china) </font></h5>
									
								</div>
								<hr />
									<h4>Data Diri</h4>
								<hr />
								<div class="form-group">
									<div class="form-group">
										<label>Nama Lengkap*</label>
										<font color = "red"><div id='myform_nama_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="nama">						
									</div>
									<div class="form-group">
										<label>Alamat di Taiwan*</label>
										<font color = "red"><div id='myform_alamat_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="alamat">						
									</div>
									<div class="form-group">
										<label>Distrik*</label>
										<font color = "red"><div id='myform_district_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="district">						
									</div>
									<div class="form-group">
										<label>Kota*</label>
										<font color = "red"><div id='myform_kota_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="kota">						
									</div>
									<div class="form-group">
										<label>Kodepos*</label>
										<font color = "red"><div id='myform_kodepos_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="kodepos">						
									</div>
									<div class="form-group">
										<label>Handphone*</label>
										<font color = "red"><div id='myform_hp_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="hp">						
									</div>
									<div class="form-group">
										<label>Email*</label>
										<font color = "red"><div id='myform_email_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="email">						
									</div>
									<div class="form-group">
										<label>Akun Facebook*</label>
										<font color = "red"><div id='myform_fb_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="fb">						
									</div>
									<div class="form-group">
										<label>No Rekening Indonesia*</label>
										<font color = "red"><div id='myform_rek_errorloc' class="error_strings"></div></font>
										<input class="form-control" name="rek">						
									</div>
									<div class="form-group">
										<label>NPWP</label>
										<input class="form-control" name="npwp">						
									</div>
									<div class="form-group">
										<label>Pangkat</label>
										<input class="form-control" name="pangkat">						
									</div>
									<hr />
									<h4>Upload Berkas <font size="3px" color="brown"><br />Silahkan upload berkas Anda di Dropbox (recommended) atau Google Drive Anda lalu share link ke kami. Harap perhatikan persmisson file supaya kami bisa download file tersebut</font></h4>
									
									<hr />
									
									<div class="form-group">
										<label>Daftar Riwayat Hidup *</label>
										<input class="form-control" name="riwayatfile">
									</div>
									<div class="form-group">
										<label>Ijazah *</label>
										<input class="form-control" name="ijazahfile">
									</div>
									<div class="form-group">
										<label>Transkip Nilai S1*</label>
										<input class="form-control"  name="transkip1file">
									</div>
									
									<div class="form-group">
										<label>Transkip Nilai S2</label>
										<input class="form-control"  name="transkip2file">
									</div>
									<div class="form-group">
										<label>Surat Kesediaan Mengajar *</label>
										<input class="form-control"  name="suratfile">
									</div>
									<div class="form-group">
										<label>Foto *</label>
										<input class="form-control"  name="fotofile">
									</div>
									
									<hr />
									<h4>Pilihan Mata Kuliah</h4>
									
									<hr />
									<div class="form-group">
										<label>Mata Kuliah 1*</label>
										<font color = "red"><div id='myform_mk1_errorloc' class="error_strings"></div></font>
										<select class="form-control" name="mk1">								
											<option selected disabled></option>
											<option value="Manajemen: Pengantar Bisnis">Manajemen: Pengantar Bisnis</option>
											<option value="Manajemen: Manajemen">Manajemen: Manajemen</option>
											<option value="Manajemen: Matematika Ekonomi">Manajemen: Matematika Ekonomi</option>
											<option value="Manajemen: Organisasi">Manajemen: Organisasi</option>
											<option value="Manajemen: Akuntansi Biaya">Manajemen: Akuntansi Biaya</option>
											<option value="Manajemen: Perilaku Konsumen">Manajemen: Perilaku Konsumen</option>
											<option value="Manajemen: Teori Portofolio dan Analisis Investasi">Manajemen: Teori Portofolio dan Analisis Investasi</option>
											<option value="Manajemen: Manajemen Kualitas">Manajemen: Manajemen Kualitas</option>
											<option value="Ilkom: Komunikasi Bisnis">Ilkom: Komunikasi Bisnis</option>
											<option value="Ilkom: Hukum Media Massa">Ilkom: Hukum Media Massa</option>
											<option value="Ilkom: Konsultasi 1">Ilkom: Konsultasi 1 </option>
											<option value="Ilkom: Konsultasi 2">Ilkom: Konsultasi 2 </option>
											<option value="Ilkom: Konsultasi 3">Ilkom: Konsultasi 3 </option>
											<option value="Ilkom: Konsultasi 4">Ilkom: Konsultasi 4 </option>
											<option value="Ilkom: Konsultasi 1">Inggris: Konsultasi 1 </option>
											<option value="Inggris: Translation III">Inggris: Translation III</option>
											<option value="Inggris: Konsultasi 2">Inggris: Konsultasi 2 </option>
											<option value="Inggris: Bahasa Indonesia Tata Bahasa dan Komposisi">Inggris: Bahasa Indonesia Tata Bahasa dan Komposisi</option>
											<option value="Inggris: Penerjemah Karya Fiksi">Inggris: Penerjemah Karya Fiksi</option>
											<option value="Inggris: Morfologi dan Sintaksis Bahasa Indonesia">Inggris: Morfologi dan Sintaksis Bahasa Indonesia</option>								
										</select>			
									</div>
									
									<div class="form-group">
										<label>Mata Kuliah 2*</label>
										<font color = "red"><div id='myform_mk2_errorloc' class="error_strings"></div></font>
										<select class="form-control" name="mk2">								
											<option selected disabled></option>
											<option selected disabled></option>
											<option value="Manajemen: Pengantar Bisnis">Manajemen: Pengantar Bisnis</option>
											<option value="Manajemen: Manajemen">Manajemen: Manajemen</option>
											<option value="Manajemen: Matematika Ekonomi">Manajemen: Matematika Ekonomi</option>
											<option value="Manajemen: Organisasi">Manajemen: Organisasi</option>
											<option value="Manajemen: Akuntansi Biaya">Manajemen: Akuntansi Biaya</option>
											<option value="Manajemen: Perilaku Konsumen">Manajemen: Perilaku Konsumen</option>
											<option value="Manajemen: Teori Portofolio dan Analisis Investasi">Manajemen: Teori Portofolio dan Analisis Investasi</option>
											<option value="Manajemen: Manajemen Kualitas">Manajemen: Manajemen Kualitas</option>
											<option value="Ilkom: Komunikasi Bisnis">Ilkom: Komunikasi Bisnis</option>
											<option value="Ilkom: Hukum Media Massa">Ilkom: Hukum Media Massa</option>
											<option value="Ilkom: Konsultasi 1">Ilkom: Konsultasi 1 </option>
											<option value="Ilkom: Konsultasi 2">Ilkom: Konsultasi 2 </option>
											<option value="Ilkom: Konsultasi 3">Ilkom: Konsultasi 3 </option>
											<option value="Ilkom: Konsultasi 4">Ilkom: Konsultasi 4 </option>
											<option value="Ilkom: Konsultasi 1">Inggris: Konsultasi 1 </option>
											<option value="Inggris: Translation III">Inggris: Translation III</option>
											<option value="Inggris: Konsultasi 2">Inggris: Konsultasi 2 </option>
											<option value="Inggris: Bahasa Indonesia Tata Bahasa dan Komposisi">Inggris: Bahasa Indonesia Tata Bahasa dan Komposisi</option>
											<option value="Inggris: Penerjemah Karya Fiksi">Inggris: Penerjemah Karya Fiksi</option>
											<option value="Inggris: Morfologi dan Sintaksis Bahasa Indonesia">Inggris: Morfologi dan Sintaksis Bahasa Indonesia</option>							
										</select>			
									</div>
									
									<button type="submit" class="btn btn-success">Daftar Tutor UTT</button>
									<div class="clearfix"></div>
							
							</form>
						<br />
						Dalam waktu 1x24 jam Anda akan mendapatkan pemberitahuan tentang aplikasi Anda, jika Anda tidak mendapatkannya, silahkan hubungi pengurusuttaiwan@gmail.com untuk konfirmasi. Terima kasih.
					</div>
				</div>               							
			</div>               							
		
		</div>
	</div>
<!-- VALIDATION SCRIPT-->
	<script language="JavaScript" type="text/javascript"
			xml:space="preserve">//<![CDATA[
		//You should create the validator only after the definition of the HTML form
	  	    var frmvalidator  = new Validator("myform");
			frmvalidator.EnableOnPageErrorDisplay();
			frmvalidator.EnableMsgsTogether();

			frmvalidator.addValidation("nama","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("alamat","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("district","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("kota","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("kodepos","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("hp","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("email","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("fb","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("rek","req","ERROR: Data harus diisi!");			
			frmvalidator.addValidation("riwayatfile","req","ERROR: Data harus diupload!");			
			frmvalidator.addValidation("riwayatfile","ijazahfile","ERROR: Data harus diupload!");			
			frmvalidator.addValidation("riwayatfile","transkip1file","ERROR: Data harus diupload!");			
			frmvalidator.addValidation("riwayatfile","suratfile","ERROR: Data harus diupload!");			
			frmvalidator.addValidation("riwayatfile","fotofile","ERROR: Data harus diupload!");			
			frmvalidator.addValidation("riwayatfile","mk1","ERROR: Kolom harus dipilih!");			
			frmvalidator.addValidation("riwayatfile","mk2","ERROR: Kolom harus dipilih!");			
			

			
		//]]>
	</script>