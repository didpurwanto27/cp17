
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<br />
					<div align="center" >
						<a href="#matkul" class="btn btn-danger">Mata Kuliah  </a>
						<a href="#syarat" class="btn btn-danger">Syarat</a>
						<a href="#untung" class="btn btn-danger">Keuntungan</a>
						<a href="#lamar" class="btn btn-danger">Pengajuan Lamaran</a>
						
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<section id = "matkul">
				<div class="alert alert-info" >
					<div class="post-image">
						<div class="post-heading">
							<h3>Persyaratan Tutor 2018.2</h3>
						</div>
					</div>
					<p>
					Universitas Terbuka-Taiwan membuka kesempatan bagi putra/putri terbaik bangsa Indonesia untuk bergabung sebagai Calon Tenaga Pengajar (Tutor) Formasi Tahun 2018.2, dengan periode pendaftaran sampai tanggal <b> 25 Juli 2018</b>.
					
					<hr />
					</p>
					<p>
					<h4>
					Formasi yang dibuka adalah:
					</h4>
					<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-cpu2{border-color:#000000;vertical-align:top}
.tg .tg-wreh{font-weight:bold;border-color:#000000;vertical-align:top}
.tg .tg-b44u{font-weight:bold;border-color:#000000}
.tg .tg-gcw3{border-color:#000000}
.tg .tg-yes0{font-weight:bold;border-color:#000000;text-align:center}
.tg .tg-mqa1{font-weight:bold;border-color:#000000;text-align:center;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-b44u" colspan="3">A. JURUSAN MANAJEMEN</th>
  </tr>
  <tr>
    <td class="tg-gcw3">1.</td>
    <td class="tg-gcw3">Teori Portofolio &amp; Analisis Investasi</td>
    <td class="tg-yes0" rowspan="4">TTM (Temu Tatap Muka)</td>
  </tr>
  <tr>
    <td class="tg-gcw3">2.</td>
    <td class="tg-gcw3">Manajemen Pemasaran</td>
  </tr>
  <tr>
    <td class="tg-gcw3">3.</td>
    <td class="tg-gcw3">Pengantar Ekonomi Mikro</td>
  </tr>
  <tr>
    <td class="tg-cpu2">4.</td>
    <td class="tg-cpu2">Pengantar Akuntansi</td>
  </tr>
  <tr>
    <td class="tg-wreh" colspan="3">B. JURUSAN ILMU KOMUNIKASI</td>
  </tr>
  <tr>
    <td class="tg-cpu2">1.</td>
    <td class="tg-cpu2">Perkembangan Teknologi Komunikasi</td>
    <td class="tg-mqa1" rowspan="3">Konsultasi 1</td>
  </tr>
  <tr>
    <td class="tg-cpu2">2.</td>
    <td class="tg-cpu2">Komunikasi Internasional</td>
  </tr>
  <tr>
    <td class="tg-cpu2">3.</td>
    <td class="tg-cpu2">Teknik Hubungan Masyarakat</td>
  </tr>
  <tr>
    <td class="tg-cpu2">1.</td>
    <td class="tg-cpu2">Komunikasi Antar Budaya</td>
    <td class="tg-mqa1" rowspan="3">Konsultasi 2</td>
  </tr>
  <tr>
    <td class="tg-cpu2">2.</td>
    <td class="tg-cpu2">Metode Penelitian Komunikasi</td>
  </tr>
  <tr>
    <td class="tg-cpu2">3.</td>
    <td class="tg-cpu2">Manajemen Media Massa</td>
  </tr>
  <tr>
    <td class="tg-cpu2">1.</td>
    <td class="tg-cpu2">Opini Publik</td>
    <td class="tg-mqa1" rowspan="5">TTM (Temu Tatap Muka)</td>
  </tr>
  <tr>
    <td class="tg-cpu2">2.</td>
    <td class="tg-cpu2">Teknik Mencari dan Menulis Berita</td>
  </tr>
  <tr>
    <td class="tg-cpu2">3.</td>
    <td class="tg-cpu2">Pengantar Statistik Sosial</td>
  </tr>
  <tr>
    <td class="tg-cpu2">4.</td>
    <td class="tg-cpu2">Pengantar Ilmu Komunikasi</td>
  </tr>
  <tr>
    <td class="tg-cpu2">5.</td>
    <td class="tg-cpu2">Hubungan Masyarakat</td>
  </tr>
  <tr>
    <td class="tg-wreh" colspan="3">C. JURUSAN BAHASA INGGRIS (PENERJEMAH)</td>
  </tr>
  <tr>
    <td class="tg-cpu2">1.</td>
    <td class="tg-cpu2">Penerjemah Karya Fiksi</td>
    <td class="tg-mqa1" rowspan="3">Konsultasi 3</td>
  </tr>
  <tr>
    <td class="tg-cpu2">2.</td>
    <td class="tg-cpu2">English MorphoSyntax</td>
  </tr>
  <tr>
    <td class="tg-cpu2">3.</td>
    <td class="tg-cpu2">Morfologi &amp; Sintaksis Bahasa Indonesia</td>
  </tr>
  <tr>
    <td class="tg-cpu2">1.</td>
    <td class="tg-cpu2">Structure II</td>
    <td class="tg-mqa1">TTM (Temu Tatap Muka)</td>
  </tr>
</table>
				</div>
				</section>
				<section id="syarat">
					<div class="alert alert-info" >
						<div class="post-heading">
							<h3>Persyaratan Tutor 2018.2</h3>
						</div>
						<ol>
							<ul>
								<li>Warga Negara Republik Indonesia, yang bertakwa kepada Tuhan Yang Maha Esa, setia dan taat kepada Pancasila, UUD Negara Republik Indonesia Tahun 1945 dan Negara Kesatuan Republik Indonesia, serta Bhinneka Tunggal Ika.</li>
								<li>	Tidak sedang atau pernah terlibat di kegiatan atau kepengurusan partai politik.</li>
								<li>	Memiliki komitmen yang tinggi untuk menjadi Tutor dan melaksanakan proses belajar-mengajar.</li>
								<li>	Mampu atau bersedia belajar mengoperasikan komputer, menggunakan perangkat lunak Office dan Online Distance Learning.</li>
								<li>	Mengikuti seluruh peraturan yang ada di Universitas Terbuka Taiwan</li>
								<li>	Memenuhi salah satu dari kriteria berikut:</li>
								<li>	Pendidikan minimal S2 dengan status guru atau minimal S1 dengan status dosen yang memiliki pengalaman mengajar minimal selama 3 tahun.</li>
								<li>	Praktisi dengan pengalaman minimal selama 5 tahun dan memiliki keahlian yang sesuai dengan mata kuliah yang akan diampu.</li>
								<li>	Pernah mengikuti mata kuliah yang akan diajarkan dengan nilai minimal B.</li>
								<li>	Bersedia mengikuti seluruh peraturan yang ada di Universitas Terbuka khususnya yang berkaitan dengan Tutor (mengikuti pelatihan Tutor, membuat RAT, SAT, absensi, nilai, CPT, dan lain-lain).</li>
								<li>	Bersedia menandatangani kontrak sebagai tutor yang diwakili oleh surat kesediaan menjadi tutor.</li>
								<li>	Bekerja dengan profesional dan bertanggung jawab</li>
								<li>	 Berdomisili di Taiwan dari September 2018 sampai Februari 2019</li>
							</ul>
						</ol>
					</div>
				</section>
				
				<section id="untung">
					<div class="alert alert-info" >
						<div class="post-heading">
							<h3>Keuntungan</h3>
						</div>
						<ul>
							<li>Pahala dari Tuhan Yang Maha Esa karena berkenan untuk membagi ilmunya</li>
							<li>Honor, sesuai standar tutor Universitas Terbuka-Jakarta (standar yang sama dengan tutor UT Indonesia di seluruh dunia)</li>
							<li>Sertifikat mengajar</li>
							<li>Surat Keputusan (SK)</li>
						</ul>
					
					</div>
				</section>
				
				<section id="lamar">
					<div class="alert alert-info" >
						<div class="post-heading">
							<h3>Pengajuan Lamaran</h3>
							<li>Seluruh berkas berikut diunggah di <a href="<?php echo base_url("index.php/pages/daftar_tutor")?>" class="btn btn-success">portal pendaftaran</a> paling lambat pada tanggal 25 Juli  2018 pukul 21.00 (GMT+08:00)</li>
						</div>
						<ul>
							<li>Mengajukan Surat Kesediaan Tutor Mengajar</li>
							<li>Photocopy Ijazah terakhir yang telah dilegalisir oleh pejabat berwenang terbaru</li>
							<li>Photocopy Transkrip Nilai S1 dan S2 yang telah dilegalisir oleh pejabat berwenang</li>
							<li>Daftar Riwayat hidup</li>
							<li>Pas Photo ukuran 4 x 6</li>
							
							
							<li> Surat Kesediaan Tutor Mengajar <a href="https://goo.gl/BbbrtT" class="btn btn-primary">  Download</a></li>
						</ul>
						
						<hr>
						<font color='red'>Untuk mendownload informasi mengenai deskripsi mata kuliah:</font>	
						<br />
						<a href="https://goo.gl/tcboij" class="btn btn-primary">Download Deskripsi Mata Kuliah</a>
						<br />
						
						<hr>
						<font color='red'>Harap untuk mendownload informasi dan dokumen tambahan berikut:</font>	
						<br />
						<a href="https://goo.gl/LwFXAJ" class="btn btn-primary">Download Dokumen Tutor UT(Penting)</a>
						<br />
						
					</div>
				</section>
					
					
					
				</div>
			</div>	
		</div>




