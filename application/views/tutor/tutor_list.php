<br />
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
        	<div class="panel-heading">
                <a href="<?php echo base_url("index.php/tutor/tutorlist")?>" class="btn btn-success">Refresh</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
								<th>No</th>
                                <th>Periode</th>                                           
								<th>Nama</th> 
                                <th>No. HP</th>          			
                                <th>Email</th>
                                <th>MK1 & MK2</th>
                                <th>Action</th> 
                            </tr>
                        </thead>
						<!-- show data table-->
						<?php
								$i=1;
								foreach($result as $data)
								{
									echo "<tr class=\"odd gradeX\" align=\"center\">";
									echo "<td>".$i."</td>";
										$i = $i+1;
									if($data['SEMESTER_DAFTAR']%2==0)
                                        //echo "<td align='left'>".$data['semester_bayar']."</td>";
                                        echo "<td align='left'><font color=\"blue\">".$data['SEMESTER_DAFTAR']."</font></td>";
                                    else
                                        echo "<td align='left'><font color=\"brown\">".$data['SEMESTER_DAFTAR']."</font></td>";
                                    echo "<td align='left'>".$data['NM_TUTOR']."</td>";
									echo "<td align='left'>".$data['HP_TUTOR']."</td>";
                                    echo "<td align='left'>".$data['EMAIL_TUTOR']."</td>";
                                    echo "<td align='left'>".$data['NM_MK1']."(".$data['ID_MK1'].")/".$data['NM_MK2']."(".$data['ID_MK2'].")</td>";
                                    echo "<td style='white-space: nowrap'><a class=\"btn btn-primary\" href=\"".base_url('index.php/tutor/detail_tutor')."/".$data['ID_TUTOR']."\"><span class=\"glyphicon glyphicon-eye-open\"></span></a>";
									echo "<a class=\"btn btn-info\" href=\"".base_url('index.php/tutor/ubah_tutor')."/".$data['ID_TUTOR']."\"> <span class=\"glyphicon glyphicon-edit\"></span></a>";
                                    echo "<a class=\"btn btn-danger\" href=\"".base_url('index.php/tutor/konfirmasi_hapus')."/".$data['ID_TUTOR']."\"> <span class=\"glyphicon glyphicon-trash\"></span></a></td>";
								}
						?>									
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

