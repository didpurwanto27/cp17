<br />
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
        	<div class="panel-heading">
                <a href="<?php echo base_url("index.php/tutor/tutorlist")?>" class="btn btn-success">Refresh</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
								<th>No</th>
                                <th>Periode</th>                                           
								<th>Nama</th> 
                                <th>No. HP/Email</th>          			
                                <th>Rekening</th> 
                                <th>MK 1</th> 
                                <th>MK 2</th> 
                                <th>Action</th> 
                            </tr>
                        </thead>
						<!-- show data table-->
						<?php
								$i=1;
								foreach($result as $data)
								{
									echo "<tr class=\"odd gradeX\" align=\"center\">";
									echo "<td>".$i."</td>";
										$i = $i+1;
									if($data['semester']%2==0)
                                        //echo "<td align='left'>".$data['semester_bayar']."</td>";
                                        echo "<td align='left'><font color=\"blue\">".$data['semester']."</font></td>";
                                    else
                                        echo "<td align='left'><font color=\"brown\">".$data['semester']."</font></td>";
                                    echo "<td align='left'>".$data['nama']."</td>";
									echo "<td align='left'>".$data['hp']."/\n".$data['email']."</td>";
                                    echo "<td align='left'>".$data['rek']."</td>";
                                    echo "<td align='left'>".$data['mk1']."</td>";
                                    echo "<td align='left'>".$data['mk2']."</td>";
                                    echo "<td><a class=\"btn btn-primary\" href=\"".base_url('index.php/tutor/detail_tutor')."/".$data['tutor_id']."\"> <font color=\"white\">Detail</font></a>";
									echo "<a class=\"btn btn-info\" href=\"".base_url('index.php/tutor/ubah_tutor')."/".$data['tutor_id']."\"> <font color=\"white\">Ubah</font></a>";
                                    echo "<a class=\"btn btn-danger\" href=\"".base_url('index.php/tutor/konfirmasi_hapus')."/".$data['tutor_id']."\"> <font color=\"white\">Hapus</font></a></td>";
								}
						?>									
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

