<div class="alert alert-default" >					
	<div style="overflow-x:auto;">

	
	<a class="btn btn-primary" href="<?php echo base_url('index.php/tutor/tutorlist')?> ">Kembali</a>
	<hr />
	<form role="form" action="<?php echo base_url("index.php/tutor/update_tutor")?>" method="POST" enctype="multipart/form-data" name="myform" id="myform">
	  <table class="table">
		<tr>
			<td width="200px">Tutor Id
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="tutor_id" readonly="true" value="<?php echo $tutor_id?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Periode
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="semester" value="<?php echo $semester?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Nama
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="nama" value="<?php echo $nama?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Alamat
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="alamat" value="<?php echo $alamat?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Distrik
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="district" value="<?php echo $district?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Kota
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="kota" value="<?php echo $kota?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Kode Pos
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="kodepos" value="<?php echo $kodepos?>">
			</td>
		<tr>
		<tr>
			<td width="200px">No. HP
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="hp" value="<?php echo $hp?>">
			</td>
		<tr>

		<tr>
			<td width="200px">Email
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="email" value="<?php echo $email?>">
			</td>
		<tr>
		
		<tr>
			<td width="200px">Facebook
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="fb" value="<?php echo $fb?>">
			</td>
		<tr>
		<tr>
			<td width="200px">NPWP
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="npwp" value="<?php echo $npwp?>">
			</td>
		<tr>
		<tr>
			<td width="200px">No. Rekening
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="rek" value="<?php echo $rek?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Pangkat
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="pangkat" value="<?php echo $pangkat?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Mata Kuliah 1
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="mk1" value="<?php echo $mk1?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Mata Kuliah 2
			</td>
			<td width="10px">:
			</td>
			<td><input class="form-control" name="mk2" value="<?php echo $mk2?>">
			</td>
		<tr>
		<tr>
			<td width="200px"><strong>Link Download</strong>
			</td>
			<td width="10px">
			</td>
			<td>
			</td>
		<tr>
		<tr>
			<td width="200px">Foto
			</td>
			<td width="10px">:
			</td>
			<td>
			<input class="form-control" name="fotofile" value="<?php echo $fotofile?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Riwayat
			</td>
			<td width="10px">:
			</td>
			<td>
			<input class="form-control" name="riwayatfile" value="<?php echo $riwayatfile?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Ijazah
			</td>
			<td width="10px">:
			</td>
			<td>
			<input class="form-control" name="ijazahfile" value="<?php echo $ijazahfile?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Transkrip 1
			</td>
			<td width="10px">:
			</td>
			<td>
			<input class="form-control" name="transkip1file" value="<?php echo $transkip1file?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Transkrip 2
			</td>
			<td width="10px">:
			</td>
			<td>
			<input class="form-control" name="transkip2file" value="<?php echo $transkip2file?>">
			</td>
		<tr>
		<tr>
			<td width="200px">Surat
			</td>
			<td width="10px">:
			</td>
			<td>
			<input class="form-control" name="suratfile" value="<?php echo $suratfile?>">
			</td>
		<tr>
	  </table>
	</div>
	<button type="submit" class="btn btn-success">Perbarui Data</button>		
	<a class="btn btn-primary" href="<?php echo base_url('index.php/tutor/tutorlist')?> ">Kembali</a>
	</form>
	<div class="clearfix"></div>

<br />
</div>



<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>