<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CP17 UTT</title>
    <link href="<?php echo base_url("public/template")?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url("public/template")?>/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>


<body>
   		<?php $this->load->view($isi); ?>  <!--LOAD PTG-->


        <script src="<?php echo base_url("public/template")?>/js/jquery-1.11.0.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/plugins/metisMenu/metisMenu.min.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/plugins/dataTables/jquery.dataTables.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
	    <script src="<?php echo base_url("public/template")?>/js/sb-admin-2.js"></script>
	    <script>
	    $(document).ready(function() {
	        $('#dataTables-example').dataTable();
	    });
	    </script>

    </div>
</body>

</html>
