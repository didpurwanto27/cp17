<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>UT Taiwan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<!-- css -->
	<link href="<?php echo base_url("public/webutt")?>/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url("public/webutt")?>/css/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="<?php echo base_url("public/webutt")?>/css/jcarousel.css" rel="stylesheet" />
	<link href="<?php echo base_url("public/webutt")?>/css/flexslider.css" rel="stylesheet" />
	<link href="<?php echo base_url("public/webutt")?>/css/style.css" rel="stylesheet" />

	<!-- Theme skin -->
	<link href="<?php echo base_url("public/webutt")?>/skins/default.css" rel="stylesheet" />

</head>

<body>
	<div id="wrapper">
		<!-- start header -->
		<header>
			<div class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
						<a class="navbar-brand" href="http://ut-taiwan.org/webutt/index.php/pages/index2"><font color="#2E3689">UT-</font></a> <a class="navbar-brand" href="http://ut-taiwan.org/webutt/index.php/pages/index2"><span><font color="orange">TAIWAN </font><font size="1" color="grey">Mandiri, Mengukir Prestasi</font></span></a> 
						
						
					</div>
					<div class="navbar-collapse collapse ">
						<ul class="nav navbar-nav">
							<li class="">
							<a href="<?php echo base_url("index.php/pages/tutor")?>" class="btn btn-info"> &nbsp Home &nbsp</a> </li>
							<li>
							<a href="<?php echo base_url("index.php/pages/syarat_tutor20181")?>" class="btn btn-info">&nbsp Syarat &nbsp</a>
							</li>						
							
							<li class=""><a href="<?php echo base_url("index.php/pages/daftar_tutor")?>" class="btn btn-info">&nbsp Daftar &nbsp</a></li>							
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		
		<div id="page-wrapper">            
			<?php $this->load->view($isi); ?>  <!--LOAD PTG-->
		</div>
		
		<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url("public/webutt")?>/js/jquery.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/jquery.easing.1.3.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/jquery.fancybox.pack.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/jquery.fancybox-media.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/google-code-prettify/prettify.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/portfolio/jquery.quicksand.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/portfolio/setting.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/jquery.flexslider.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/animate.js"></script>
	<script src="<?php echo base_url("public/webutt")?>/js/custom.js"></script>
		