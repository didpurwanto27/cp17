<?php

class Pages extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->model('momodel','modelp');	
		$this->load->library('excel');		
		
	}
	
	function login_action(){
		$email = $this->input->post('email');
		$passwd = $this->input->post('passwd');
		
		$previlage = $this->modelp->cekData($email, md5($passwd));
		if($previlage==true){
			$this->session->set_userdata('email', $email);
			redirect('pages/home');
		}
		else{
			redirect(base_url(),'refresh');
		}
	}
	
	function checkSession(){
	    if(!$this->session->userdata('email'))
	    {
	    	redirect(base_url(),'refresh');
	   	}
	}
	
	function logout(){
		$this->session->sess_destroy();
   		redirect(base_url(),'refresh');
	}
	
	function index(){	
		redirect('pages/index2');
	}
	
	function index2(){
		$this->load->view('index');
	}
	
	function home(){
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$data['isi'] = "home";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);	
		}
	}
	
	



	/*function download_pembayaran(){
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$data['result'] = $this->modelp->cekAngkatan();
			foreach($data['result'] as $rows){
				$data['ANGKATAN_MHS'] 	= $rows['ANGKATAN_MHS'];				
			}				
			$data['isi'] = "download_pembayaran";
			
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);	
		}
	}
	
	function download_mahasiswa(){
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$data['result'] = $this->modelp->cekAngkatan();
			foreach($data['result'] as $rows){
				$data['ANGKATAN_MHS'] 	= $rows['ANGKATAN_MHS'];				
			}
			$data['result_jurusan'] = $this->modelp->cekJurusan();
			foreach($data['result_jurusan'] as $rows){
				$data['ID_PROGSTUDI'] 	= $rows['ID_PROGSTUDI'];				
				$data['NAMA_PROGSTUDI'] = $rows['NAMA_PROGSTUDI'];				
			}
			$data['isi'] = "download_mahasiswa";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);	
		}
	}
	
	
	
	
	
	function export_data() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			//$this->load->view('export');
			$data['dataku'] = $this->modelp->cekxls();
			$this->load->view('export',$data);
		}
    }
	
	
	//=====================================================
	// EXPORT FILE
	//=====================================================
    function createXLS() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			// create file name
			$fileName = 'data-'.time().'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$empInfo = $this->modelp->cekxls();
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'NAMA_MHS');
			// set Row
			$rowCount = 2;
			foreach ($empInfo as $element) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ID_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['NAMA_MHS']);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('uploads/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			$temp = base_url() . "/uploads/" .$fileName;
			redirect($temp); 
		}			
    }
	
	function createXLS_rekapbayar() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$tahun = $this->input->post('tahun');
			// create file name
			$fileName = 'pembayaran-'.time().'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$empInfo = $this->modelp->cekxls1($tahun);
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'SEMESTER_ID');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ANGKATAN_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'MAHASISWA_ID');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'NIM_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'NAMA_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'TELP_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'NAMAFB_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'SEMESTER_AKTIF');
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'TGL_PEMBAYARAN');
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'conf_status');
			$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'atas_nm');
			$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'no_tlp');
			$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'UO');
			$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'UK');
			$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'UU');
			// set Row
			$rowCount = 2;
			foreach ($empInfo as $element) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['SEMESTER_ID']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['ANGKATAN_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['MAHASISWA_ID']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['NIM_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['NAMAFB_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TELP_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['NAMAFB_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['SEMESTER_AKTIF']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['TGL_PEMBAYARAN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['conf_status']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['atas_nm']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['no_tlp']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['UO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['UK']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['UU']);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('uploads/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			$temp = base_url() . "/uploads/" .$fileName;
			redirect($temp); 
		}			
    }
	
	function createXLS2() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$tahun = $this->input->post('tahun');
			// create file name
			$fileName = 'mahasiswa-angkatan-'.time().'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$empInfo = $this->modelp->cekxls2($tahun);
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'NAMA_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ALAMAT_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'TELP_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'CELLPHONE_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'EMAIL_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'TGL_LHR_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'TMP_LHR_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'AGAMA_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'NOREKENING_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'NAMAFB_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'ANGKATAN_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'AKTIF');
			$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'SEMESTER_AKTIF');
			
			// set Row
			$rowCount = 2;
			foreach ($empInfo as $element) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ID_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['NAMA_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['ALAMAT_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TELP_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['CELLPHONE_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['EMAIL_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['TGL_LHR_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['TMP_LHR_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['AGAMA_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['NOREKENING_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['NAMAFB_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['ANGKATAN_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['AKTIF']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['SEMESTER_AKTIF']);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('uploads/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			$temp = base_url() . "/uploads/" .$fileName;
			redirect($temp); 
		}			
    }
	
	function createXLS3() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$jurusan = $this->input->post('jurusan');
			// create file name
			$fileName = 'mahasiswa-jurusan-'.time().'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$empInfo = $this->modelp->cekxls3($jurusan);
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'NAMA_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ALAMAT_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'TELP_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'CELLPHONE_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'EMAIL_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'TGL_LHR_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'TMP_LHR_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'AGAMA_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'NOREKENING_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'NAMAFB_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'ANGKATAN_MHS');
			$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'AKTIF');
			$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'SEMESTER_AKTIF');
			
			// set Row
			$rowCount = 2;
			foreach ($empInfo as $element) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ID_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['NAMA_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['ALAMAT_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TELP_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['CELLPHONE_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['EMAIL_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['TGL_LHR_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['TMP_LHR_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['AGAMA_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['NOREKENING_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['NAMAFB_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['ANGKATAN_MHS']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['AKTIF']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['SEMESTER_AKTIF']);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('uploads/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			$temp = base_url() . "/uploads/" .$fileName;
			redirect($temp); 
		}			
    }*/




	
	
	
	
	
	




	




	
}	
