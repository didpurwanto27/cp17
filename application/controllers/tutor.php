<?php

class Tutor extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->model('momodel','modelp');	
		$this->load->library('excel');		
		
	}

	function checkSession(){
	    if(!$this->session->userdata('email'))
	    {
	    	redirect(base_url(),'refresh');
	   	}
	}
	
	function login_action(){
		$email = $this->input->post('email');
		$passwd = $this->input->post('passwd');
		
		$previlage = $this->modelp->cekData($email, md5($passwd));
		if($previlage==true){
			$this->session->set_userdata('email', $email);
			redirect('pages/home');
		}
		else{
			redirect(base_url(),'refresh');
		}
	}
    
	
	function tutorlist(){
		$this->checkSession();
		if($this->session->userdata('email')){	
			$data['result'] = $this->modelp->selectAllTutor();
			foreach ($data['result'] as $rows) {
				$data['SEMESTER_DAFTAR']	 = $rows['SEMESTER_DAFTAR'];
				$data['ID_TUTOR']	 = $rows['ID_TUTOR'];
				$data['ALAMAT_TUTOR']	 = $rows['ALAMAT_TUTOR'];
				$data['HP_TUTOR']	 = $rows['HP_TUTOR'];
				$data['EMAIL_TUTOR']	 = $rows['EMAIL_TUTOR'];
				$data['REK_TUTOR']	 = $rows['REK_TUTOR'];
				$data['ID_MK1']	 = $rows['ID_MK1'];
				$data['ID_MK2']	 = $rows['ID_MK2'];
				$data['NM_MK1']	 = $rows['NM_MK1'];
				$data['NM_MK2']	 = $rows['NM_MK2'];
			}
			$data['isi'] = "tutor/tutor_list";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
		}	
	}
	
	function detail_tutor(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->getID_TUTOR($data['id']);
			foreach($data['result'] as $rows)
			{
				$data['SEMESTER_DAFTAR']	 = $rows['SEMESTER_DAFTAR'];
				$data['ID_TUTOR']	 = $rows['ID_TUTOR'];
				$data['NM_TUTOR']	 = $rows['NM_TUTOR'];
				$data['ALAMAT_TUTOR']	 = $rows['ALAMAT_TUTOR'];
				$data['HP_TUTOR']	 = $rows['HP_TUTOR'];
				$data['EMAIL_TUTOR']	 = $rows['EMAIL_TUTOR'];
				$data['REK_TUTOR']	 = $rows['REK_TUTOR'];
				$data['ID_MK1']	 = $rows['ID_MK1'];
				$data['ID_MK2']	 = $rows['ID_MK2'];		
				$data['DISTRIK_TUTOR'] 	= $rows['DISTRIK_TUTOR'];		
				$data['KOTA_TUTOR'] 	= $rows['KOTA_TUTOR'];		
				$data['KDPOS_TUTOR'] 	= $rows['KDPOS_TUTOR'];		
				$data['FB_TUTOR'] 	= $rows['FB_TUTOR'];		
				$data['NPWP_TUTOR'] 	= $rows['NPWP_TUTOR'];	
				$data['PANGKAT_TUTOR'] 	= $rows['PANGKAT_TUTOR'];		
				$data['RIWAYAT_HIDUP_TUTOR'] 	= $rows['RIWAYAT_HIDUP_TUTOR'];				
				$data['IJAZAH_TUTOR'] 	= $rows['IJAZAH_TUTOR'];		
				$data['TRANSKIP_TUTOR'] 	= $rows['TRANSKIP_TUTOR'];		
				$data['TRANSKIP_S2_TUTOR'] 	= $rows['TRANSKIP_S2_TUTOR'];		
				$data['SKM_TUTOR'] 	= $rows['SKM_TUTOR'];		
				$data['FOTO_TUTOR'] 	= $rows['FOTO_TUTOR'];
				$data['NM_MK1']	 = $rows['NM_MK1'];
				$data['NM_MK2']	 = $rows['NM_MK2'];
			}

			$data['isi'] = "tutor/detail_tutor";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
			
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function konfirmasi_hapus(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['isi'] = "dialog";
			$data['execute'] = "index.php/tutor/hapus_data/".$data['id'];
			$data['redirect'] = "index.php/tutor/tutorlist";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function hapus_data(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			if($this->modelp->hapusID_TUTOR($data['id']) == false ){
			   $data['pesan'] = "Hapus Gagal!";
			   $data['status'] = false;
			} 
			else{
			   $data['pesan'] = "Hapus Berhasil!";
			   $data['status'] = true;
			}
			$data['isi'] = "notification";
			$data['redirect'] = "index.php/tutor/tutorlist";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function ubah_tutor(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->getID_TUTOR($data['id']);
			foreach($data['result'] as $rows)
			{
				$data['SEMESTER_DAFTAR']	 = $rows['SEMESTER_DAFTAR'];
				$data['ID_TUTOR']	 = $rows['ID_TUTOR'];
				$data['NM_TUTOR']	 = $rows['NM_TUTOR'];
				$data['ALAMAT_TUTOR']	 = $rows['ALAMAT_TUTOR'];
				$data['HP_TUTOR']	 = $rows['HP_TUTOR'];
				$data['EMAIL_TUTOR']	 = $rows['EMAIL_TUTOR'];
				$data['REK_TUTOR']	 = $rows['REK_TUTOR'];
				$data['ID_MK1']	 = $rows['ID_MK1'];
				$data['ID_MK2']	 = $rows['ID_MK2'];		
				$data['DISTRIK_TUTOR'] 	= $rows['DISTRIK_TUTOR'];		
				$data['KOTA_TUTOR'] 	= $rows['KOTA_TUTOR'];		
				$data['KDPOS_TUTOR'] 	= $rows['KDPOS_TUTOR'];		
				$data['FB_TUTOR'] 	= $rows['FB_TUTOR'];		
				$data['NPWP_TUTOR'] 	= $rows['NPWP_TUTOR'];	
				$data['PANGKAT_TUTOR'] 	= $rows['PANGKAT_TUTOR'];		
				$data['RIWAYAT_HIDUP_TUTOR'] 	= $rows['RIWAYAT_HIDUP_TUTOR'];				
				$data['IJAZAH_TUTOR'] 	= $rows['IJAZAH_TUTOR'];		
				$data['TRANSKIP_TUTOR'] 	= $rows['TRANSKIP_TUTOR'];		
				$data['TRANSKIP_S2_TUTOR'] 	= $rows['TRANSKIP_S2_TUTOR'];		
				$data['SKM_TUTOR'] 	= $rows['SKM_TUTOR'];		
				$data['FOTO_TUTOR'] 	= $rows['FOTO_TUTOR'];	
				$data['NM_MK1']	 = $rows['NM_MK1'];
				$data['NM_MK2']	 = $rows['NM_MK2'];
			}
			$data['MK'] = $this->modelp->getMatKul();
			foreach($data['MK'] as $rows)
			{
				$data['ID_MK']	 = $rows['NAMA_MK'];
				$data['NAMA_MK'] = $rows['NAMA_MK'];
			}
			$data['isi'] = "tutor/ubah_tutor";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
			
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function update_tutor(){
		$ID_TUTOR = $this->input->post('ID_TUTOR');
		$SEMESTER_DAFTAR = $this->input->post('SEMESTER_DAFTAR');
		$NM_TUTOR = $this->input->post('NM_TUTOR');
		$ALAMAT_TUTOR = $this->input->post('ALAMAT_TUTOR');
		$DISTRIK_TUTOR = $this->input->post('DISTRIK_TUTOR');
		$KOTA_TUTOR = $this->input->post('KOTA_TUTOR');
		$KDPOS_TUTOR = $this->input->post('KDPOS_TUTOR');
		$HP_TUTOR_TUTOR = $this->input->post('HP_TUTOR');
		$EMAIL_TUTOR = $this->input->post('EMAIL_TUTOR');
		$FB_TUTOR = $this->input->post('FB_TUTOR');
		$REK_TUTOR = $this->input->post('REK_TUTOR');
		$NPWP_TUTOR = $this->input->post('NPWP_TUTOR');
		$PANGKAT_TUTOR = $this->input->post('PANGKAT_TUTOR');
		
		$RIWAYAT_HIDUP_TUTOR = $this->input->post('RIWAYAT_HIDUP_TUTOR');
		$IJAZAH_TUTOR = $this->input->post('IJAZAH_TUTOR');
		$TRANSKIP_TUTOR = $this->input->post('TRANSKIP_TUTOR');
		$TRANSKIP_S2_TUTOR = $this->input->post('TRANSKIP_S2_TUTOR');
		$SKM_TUTOR = $this->input->post('SKM_TUTOR');
		$FOTO_TUTOR = $this->input->post('FOTO_TUTOR');
		$ID_MK1 = $this->input->post('ID_MK1');
		$ID_MK2 = $this->input->post('ID_MK2');
		$this->checkSession();
		if($this->session->userdata('email')){
			if($this->modelp->updateTutor($ID_TUTOR,$SEMESTER_DAFTAR,$NM_TUTOR,$ALAMAT_TUTOR,$HP_TUTOR_TUTOR,$EMAIL_TUTOR,$REK_TUTOR,$ID_MK1,$ID_MK2,$DISTRIK_TUTOR,$KOTA_TUTOR,$KDPOS_TUTOR,$FB_TUTOR,$NPWP_TUTOR,$PANGKAT_TUTOR,$RIWAYAT_HIDUP_TUTOR,$IJAZAH_TUTOR,$TRANSKIP_TUTOR,$TRANSKIP_S2_TUTOR,$SKM_TUTOR,$FOTO_TUTOR) == false ){
			   $data['pesan'] = "Ubah Gagal!";
			   $data['status'] = false;
			   $data['redirect'] = "index.php/tutor/ubah_tutor/".$ID_TUTOR;
			} 
			else{
			   $data['pesan'] = "Ubah Berhasil!";
			   $data['status'] = true;
			   $data['redirect'] = "index.php/tutor/tutorlist";
			}
			$data['isi'] = "notification";
			
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
		else{
			echo $this->upload->display_errors();
		}
		
	}
	
	function syarat_tutor20181()
	{
		$data['isi'] = "tutor/syarat_tutor20181";
		$this->load->view('sidebar3', $data);	
		
	}
	
	
	function daftar_tutor()
	{
		$data['isi'] = "tutor/daftar_tutor";
		$this->load->view('sidebar3', $data);	
	}
	
	function insert_tutor(){
			$SEMESTER_DAFTAR = '20181'; // change the SEMESTER_DAFTAR
			$NM_TUTOR = $this->input->post('NM_TUTOR');
			$ALAMAT_TUTOR = $this->input->post('ALAMAT_TUTOR');
			$DISTRIK_TUTOR = $this->input->post('DISTRIK_TUTOR');
			$KOTA_TUTOR = $this->input->post('KOTA_TUTOR');
			$KDPOS_TUTOR = $this->input->post('KDPOS_TUTOR');
			$HP_TUTOR_TUTOR = $this->input->post('HP_TUTOR');
			$EMAIL_TUTOR = $this->input->post('EMAIL_TUTOR');
			$FB_TUTOR = $this->input->post('FB_TUTOR');
			$REK_TUTOR = $this->input->post('REK_TUTOR');
			$NPWP_TUTOR = $this->input->post('NPWP_TUTOR');
			$PANGKAT_TUTOR = $this->input->post('PANGKAT_TUTOR');
			
			$RIWAYAT_HIDUP_TUTOR = $this->input->post('RIWAYAT_HIDUP_TUTOR');
			$IJAZAH_TUTOR = $this->input->post('IJAZAH_TUTOR');
			$TRANSKIP_TUTOR = $this->input->post('TRANSKIP_TUTOR');
			$TRANSKIP_S2_TUTOR = $this->input->post('TRANSKIP_S2_TUTOR');
			$SKM_TUTOR = $this->input->post('SKM_TUTOR');
			$FOTO_TUTOR = $this->input->post('FOTO_TUTOR');
			$ID_MK1 = $this->input->post('ID_MK1');
			$ID_MK2 = $this->input->post('ID_MK2');
			
			$this->modelp->inserttutor($SEMESTER_DAFTAR, $NM_TUTOR, $ALAMAT_TUTOR, $DISTRIK_TUTOR, $KOTA_TUTOR, $KDPOS_TUTOR, $HP_TUTOR_TUTOR, $EMAIL_TUTOR, $FB_TUTOR, $REK_TUTOR, $NPWP_TUTOR, $PANGKAT_TUTOR, $RIWAYAT_HIDUP_TUTOR, $IJAZAH_TUTOR, $TRANSKIP_TUTOR, $TRANSKIP_S2_TUTOR, $SKM_TUTOR, $FOTO_TUTOR, $ID_MK1, $ID_MK2);		
			
			redirect('pages/sukses');
		
	}
	
	function sukses()
	{
		$this->load->view('tutor/sukses');	
	}
	
	function tutor()
	{
		$this->load->view('tutor/tutor');	
	}
	
	function data_tutor(){
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$data['result'] = $this->modelp->cekSemesterTutor();
			foreach($data['result'] as $rows){
				$data['SEMESTER_DAFTAR'] 	= $rows['SEMESTER_DAFTAR'];				
			}
			$data['isi'] = "tutor/data_tutor";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
		}
	}
	
	function createXLS_datatutor() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$SEMESTER_DAFTAR = $this->input->post('SEMESTER_DAFTAR');
			// create file name
			$fileName = 'tutor-angkatan-'.time().'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$empInfo = $this->modelp->cekxls_tutor($SEMESTER_DAFTAR);
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'SEMESTER_DAFTAR');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'NM_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ALAMAT_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'DISTRIK_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'KOTA_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'KDPOS_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'HP_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'EMAIL_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'FB_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'REK_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'NPWP_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'PANGKAT_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'RIWAYAT_HIDUP_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'IJAZAH_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'TRANSKIP_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'TRANSKIP_S2_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'SKM_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'FOTO_TUTOR');
			$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'ID_MK1');
			$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'ID_MK2');
			$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'TIME');
			
			// set Row
			$rowCount = 2;
			foreach ($empInfo as $element) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['SEMESTER_DAFTAR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['NM_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['ALAMAT_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['DISTRIK_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['KOTA_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['KDPOS_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['HP_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['EMAIL_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['FB_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['REK_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['NPWP_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['PANGKAT_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['RIWAYAT_HIDUP_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['IJAZAH_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['TRANSKIP_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['TRANSKIP_S2_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['SKM_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $element['FOTO_TUTOR']);
				$objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $element['ID_MK1']);
				$objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $element['ID_MK2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $element['TIME']);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('uploads/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			$temp = base_url() . "/uploads/" .$fileName;
			redirect($temp); 
		}			
    }
	
	
}
?>
