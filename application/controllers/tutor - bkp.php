<?php

class Tutor extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->model('momodel','modelp');	
		$this->load->library('excel');		
		
	}

	function checkSession(){
	    if(!$this->session->userdata('email'))
	    {
	    	redirect(base_url(),'refresh');
	   	}
	}
	
	function login_action(){
		$email = $this->input->post('email');
		$passwd = $this->input->post('passwd');
		
		$previlage = $this->modelp->cekData($email, md5($passwd));
		if($previlage==true){
			$this->session->set_userdata('email', $email);
			redirect('pages/home');
		}
		else{
			redirect(base_url(),'refresh');
		}
	}
	
	function tutorlist(){
		$this->checkSession();
		if($this->session->userdata('email')){	
			$data['result'] = $this->modelp->selectAllTutor();
			foreach ($data['result'] as $rows) {
				$data['semester']	 = $rows['semester'];
				$data['tutor_id']	 = $rows['tutor_id'];
				$data['alamat']	 = $rows['alamat'];
				$data['hp']	 = $rows['hp'];
				$data['email']	 = $rows['email'];
				$data['rek']	 = $rows['rek'];
				$data['mk1']	 = $rows['mk1'];
				$data['mk2']	 = $rows['mk2'];
			}
			$data['isi'] = "tutor/tutor_list";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
		}	
	}
	
	function detail_tutor(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->getTutor_id($data['id']);
			foreach($data['result'] as $rows)
			{
				$data['semester']	 = $rows['semester'];
				$data['tutor_id']	 = $rows['tutor_id'];
				$data['nama']	 = $rows['nama'];
				$data['alamat']	 = $rows['alamat'];
				$data['hp']	 = $rows['hp'];
				$data['email']	 = $rows['email'];
				$data['rek']	 = $rows['rek'];
				$data['mk1']	 = $rows['mk1'];
				$data['mk2']	 = $rows['mk2'];		
				$data['district'] 	= $rows['district'];		
				$data['kota'] 	= $rows['kota'];		
				$data['kodepos'] 	= $rows['kodepos'];		
				$data['fb'] 	= $rows['fb'];		
				$data['npwp'] 	= $rows['npwp'];	
				$data['pangkat'] 	= $rows['pangkat'];		
				$data['riwayatfile'] 	= $rows['riwayatfile'];				
				$data['ijazahfile'] 	= $rows['ijazahfile'];		
				$data['transkip1file'] 	= $rows['transkip1file'];		
				$data['transkip2file'] 	= $rows['transkip2file'];		
				$data['suratfile'] 	= $rows['suratfile'];		
				$data['fotofile'] 	= $rows['fotofile'];	
			}

			$data['isi'] = "tutor/detail_tutor";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
			
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function konfirmasi_hapus(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['isi'] = "dialog";
			$data['execute'] = "index.php/tutor/hapus_data/".$data['id'];
			$data['redirect'] = "index.php/tutor/tutorlist";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function hapus_data(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			if($this->modelp->hapusTutor_id($data['id']) == false ){
			   $data['pesan'] = "Hapus Gagal!";
			   $data['status'] = false;
			} 
			else{
			   $data['pesan'] = "Hapus Berhasil!";
			   $data['status'] = true;
			}
			$data['isi'] = "notification";
			$data['redirect'] = "index.php/tutor/tutorlist";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function ubah_tutor(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->getTutor_id($data['id']);
			foreach($data['result'] as $rows)
			{
				$data['semester']	 = $rows['semester'];
				$data['tutor_id']	 = $rows['tutor_id'];
				$data['nama']	 = $rows['nama'];
				$data['alamat']	 = $rows['alamat'];
				$data['hp']	 = $rows['hp'];
				$data['email']	 = $rows['email'];
				$data['rek']	 = $rows['rek'];
				$data['mk1']	 = $rows['mk1'];
				$data['mk2']	 = $rows['mk2'];		
				$data['district'] 	= $rows['district'];		
				$data['kota'] 	= $rows['kota'];		
				$data['kodepos'] 	= $rows['kodepos'];		
				$data['fb'] 	= $rows['fb'];		
				$data['npwp'] 	= $rows['npwp'];	
				$data['pangkat'] 	= $rows['pangkat'];		
				$data['riwayatfile'] 	= $rows['riwayatfile'];				
				$data['ijazahfile'] 	= $rows['ijazahfile'];		
				$data['transkip1file'] 	= $rows['transkip1file'];		
				$data['transkip2file'] 	= $rows['transkip2file'];		
				$data['suratfile'] 	= $rows['suratfile'];		
				$data['fotofile'] 	= $rows['fotofile'];	
			}

			$data['isi'] = "tutor/ubah_tutor";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
			
		}
		else{
			echo $this->upload->display_errors();
		}
	}
	
	function update_tutor(){
		$tutor_id = $this->input->post('tutor_id');
		$semester = $this->input->post('semester');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$district = $this->input->post('district');
		$kota = $this->input->post('kota');
		$kodepos = $this->input->post('kodepos');
		$hp = $this->input->post('hp');
		$email = $this->input->post('email');
		$fb = $this->input->post('fb');
		$rek = $this->input->post('rek');
		$npwp = $this->input->post('npwp');
		$pangkat = $this->input->post('pangkat');
		
		$riwayatfile = $this->input->post('riwayatfile');
		$ijazahfile = $this->input->post('ijazahfile');
		$transkip1file = $this->input->post('transkip1file');
		$transkip2file = $this->input->post('transkip2file');
		$suratfile = $this->input->post('suratfile');
		$fotofile = $this->input->post('fotofile');
		$mk1 = $this->input->post('mk1');
		$mk2 = $this->input->post('mk2');
		$this->checkSession();
		if($this->session->userdata('email')){
			if($this->modelp->updateTutor($tutor_id,$semester,$nama,$alamat,$hp,$email,$rek,$mk1,$mk2,$district,$kota,$kodepos,$fb,$npwp,$pangkat,$riwayatfile,$ijazahfile,$transkip1file,$transkip2file,$suratfile,$fotofile) == false ){
			   $data['pesan'] = "Ubah Gagal!";
			   $data['status'] = false;
			   $data['redirect'] = "index.php/tutor/ubah_tutor/".$tutor_id;
			} 
			else{
			   $data['pesan'] = "Ubah Berhasil!";
			   $data['status'] = true;
			   $data['redirect'] = "index.php/tutor/tutorlist";
			}
			$data['isi'] = "notification";
			
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
		else{
			echo $this->upload->display_errors();
		}
		
	}
	
	function syarat_tutor20181()
	{
		$data['isi'] = "tutor/syarat_tutor20181";
		$this->load->view('sidebar3', $data);	
		
	}
	
	
	function daftar_tutor()
	{
		$data['isi'] = "tutor/daftar_tutor";
		$this->load->view('sidebar3', $data);	
	}
	
	function insert_tutor(){
			$semester = '20181'; // change the semester
			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			$district = $this->input->post('district');
			$kota = $this->input->post('kota');
			$kodepos = $this->input->post('kodepos');
			$hp = $this->input->post('hp');
			$email = $this->input->post('email');
			$fb = $this->input->post('fb');
			$rek = $this->input->post('rek');
			$npwp = $this->input->post('npwp');
			$pangkat = $this->input->post('pangkat');
			
			$riwayatfile = $this->input->post('riwayatfile');
			$ijazahfile = $this->input->post('ijazahfile');
			$transkip1file = $this->input->post('transkip1file');
			$transkip2file = $this->input->post('transkip2file');
			$suratfile = $this->input->post('suratfile');
			$fotofile = $this->input->post('fotofile');
			$mk1 = $this->input->post('mk1');
			$mk2 = $this->input->post('mk2');
			
			$this->modelp->inserttutor($semester, $nama, $alamat, $district, $kota, $kodepos, $hp, $email, $fb, $rek, $npwp, $pangkat, $riwayatfile, $ijazahfile, $transkip1file, $transkip2file, $suratfile, $fotofile, $mk1, $mk2);		
			
			redirect('pages/sukses');
		
	}
	
	function sukses()
	{
		$this->load->view('tutor/sukses');	
	}
	
	function tutor()
	{
		$this->load->view('tutor/tutor');	
	}
	
	function data_tutor(){
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$data['result'] = $this->modelp->cekSemesterTutor();
			foreach($data['result'] as $rows){
				$data['SEMESTER'] 	= $rows['SEMESTER'];				
			}
			$data['isi'] = "tutor/data_tutor";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
		}
	}
	
	function createXLS_datatutor() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$semester = $this->input->post('semester');
			// create file name
			$fileName = 'tutor-angkatan-'.time().'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$empInfo = $this->modelp->cekxls_tutor($semester);
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'semester');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'nama');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'alamat');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'district');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'kota');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'kodepos');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'hp');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'email');
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'fb');
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'rek');
			$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'npwp');
			$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'pangkat');
			$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'riwayatfile');
			$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'ijazahfile');
			$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'transkip1file');
			$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'transkip2file');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'suratfile');
			$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'fotofile');
			$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'mk1');
			$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'mk2');
			$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'waktu');
			
			// set Row
			$rowCount = 2;
			foreach ($empInfo as $element) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['semester']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['nama']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['alamat']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['district']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['kota']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['kodepos']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['hp']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['email']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['fb']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['rek']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['npwp']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['pangkat']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['riwayatfile']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['ijazahfile']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['transkip1file']);
				$objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['transkip2file']);
				$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['suratfile']);
				$objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $element['fotofile']);
				$objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $element['mk1']);
				$objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $element['mk2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $element['waktu']);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('uploads/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			$temp = base_url() . "/uploads/" .$fileName;
			redirect($temp); 
		}			
    }
	
	
}