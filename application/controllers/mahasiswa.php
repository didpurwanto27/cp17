<?php

class Mahasiswa extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->model('momodel','modelp');	
		$this->load->library('excel');		
		
	}

	function checkSession(){
	    if(!$this->session->userdata('email'))
	    {
	    	redirect(base_url(),'refresh');
	   	}
	}
	
	function login_action(){
		$email = $this->input->post('email');
		$passwd = $this->input->post('passwd');
		
		$previlage = $this->modelp->cekData($email, md5($passwd));
		if($previlage==true){
			$this->session->set_userdata('email', $email);
			redirect('pages/home');
		}
		else{
			redirect(base_url(),'refresh');
		}
	}
    
	function list_mahasiswa(){
		$this->checkSession();
		if($this->session->userdata('email')){	
			$data['isi'] = "mahasiswa/list_mahasiswa";
			$data['result'] = $this->modelp->selectAllMahasiswa();
			// print_r($data['result']);
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}	
	}

	function detail_mahasiswa($id_mhs) {
		$this->checkSession();
		if($this->session->userdata('email')){	
			$data['isi'] = "mahasiswa/detail_mahasiswa";
			$result = $this->modelp->selectMahasiswaById($id_mhs);
			// print_r($result);
			$data['detail'] = $result[0];
			// print_r($data['detail']);
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}	
	}

	function add_mahasiswa() {
		$this->checkSession();
		if($this->session->userdata('email')){	
			$data['isi'] = "mahasiswa/new_mahasiswa";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
	}

	function do_add_mahasiswa(){
		$this->checkSession();
		if($this->session->userdata('email')){
			$data = array(
				'nim_mhs' => $this->input->post('nim'),
				'nama_mhs' => $this->input->post('nama'),
				'alamat_mhs' => $this->input->post('alamat'),
				'district_mhs' => $this->input->post('district'),
				'kabkot_mhs' => $this->input->post('kabkot'),
				'kodepos_mhs' => $this->input->post('kode_pos'),
				'telp_mhs' => $this->input->post('telp'),
				'cellphone_mhs' => $this->input->post('hp'),
				'email_mhs' => $this->input->post('email'),
				'tmp_lhr_mhs' => $this->input->post('tmplahir'),
				'tgl_lhr_mhs' => $this->input->post('tgllahir'),
				'agama_mhs' => $this->input->post('agama'),
				'progstudi_id' => $this->input->post('prodi'),
				'kdupbjj_mhs' => $this->input->post('upbjj'),
				'jk_mhs' => $this->input->post('jk'),
				'wn_mhs' => $this->input->post('warganegara'),
				'pekerjaan_mhs' => $this->input->post('pekerjaan'),
				'stkwn_mhs' => $this->input->post('kawin'),
				'id_tempatujian' => $this->input->post('tempatujian'),
				'jenjangpdk_mhs' => $this->input->post('jenjang'),
				'jurusan_mhs' => $this->input->post('jurusan'),
				'thnijazah_mhs' => $this->input->post('thnijazah'),
				'namaibu_mhs' => $this->input->post('nama_ibu'),
				'norekening_mhs' => $this->input->post('rekening'),
				'bank_mhs' => $this->input->post('bank'),
				'namafb_mhs' => $this->input->post('fb'),
				'angkatan_mhs' => $this->input->post('angkatan'),
				'semester_aktif' => $this->input->post('semester'),
				'aktif' => $this->input->post('status')
			);
			$result = $this->modelp->insertMahasiswa($data);
			print_r($result);

			redirect('mahasiswa/list_mahasiswa');
		}
	}

	function delete_mahasiswa($id_mhs) {
		$this->checkSession();
		if($this->session->userdata('email')){	
			$result = $this->modelp->deleteMahasiswaById($id_mhs);
			redirect('mahasiswa/list_mahasiswa');
		}
	}

	function edit_mahasiswa($id_mhs) {
		# code...
	}


}
