<?php

class Pembayaran extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->model('momodel','modelp');	
		$this->load->library('excel');		
		
	}

	function checkSession(){
	    if(!$this->session->userdata('email'))
	    {
	    	redirect(base_url(),'refresh');
	   	}
	}
	
	function login_action(){
		$email = $this->input->post('email');
		$passwd = $this->input->post('passwd');
		
		$previlage = $this->modelp->cekData($email, md5($passwd));
		if($previlage==true){
			$this->session->set_userdata('email', $email);
			redirect('pages/home');
		}
		else{
			redirect(base_url(),'refresh');
		}
	}

	function homebayar(){
		$this->checkSession();
		if($this->session->userdata('email')){	
			$data['result'] = $this->modelp->selectPembayaran();
			foreach ($data['result'] as $rows) {
				$data['bayar_id']	 = $rows['bayar_id'];
				$data['semester_bayar']	 = $rows['semester_bayar'];
				$data['nim']	 = $rows['nim'];
				$data['nama']	 = $rows['nama'];
				$data['jurusan']	 = $rows['jurusan'];
				$data['semester']	 = $rows['semester'];
				$data['metode']	 = $rows['metode'];
				$data['jumlah']	 = $rows['jumlah'];
				$data['status']	 = $rows['status'];
			}
			$data['isi'] = "pembayaran/pembayaran";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
		}	
	}

	
	function konfirmasi_bayar(){
		$this->load->view('konfirmasi/konf');
	}

	function konf_page(){
		$data['result'] = $this->modelp->cekAngkatan();
		foreach($data['result'] as $rows)
		{
			$data['ANGKATAN_MHS'] 	= $rows['ANGKATAN_MHS'];
		}

		//$data['isi'] = "konfirmasi/show_konf";				
		$this->load->view('konfirmasi/konf_page', $data);	
	}

	function show_konf(){
		$this->load->view('konfirmasi/show_konf');
	}

	function status_pembayaran(){
		$semester = '20191'; // change every semester ya
		$data['result'] = $this->modelp->selectPembayaranSemester($semester);
		foreach ($data['result'] as $rows) {
			$data['bayar_id']	 = $rows['bayar_id'];
			$data['semester_bayar']	 = $rows['semester_bayar'];
			$data['nim']	 = $rows['nim'];
			$data['nama']	 = $rows['nama'];
			$data['jurusan']	 = $rows['jurusan'];
			$data['semester']	 = $rows['semester'];
			$data['metode']	 = $rows['metode'];
			$data['jumlah']	 = $rows['jumlah'];
			$data['status']	 = $rows['status'];
		}
		$data['isi'] = "pembayaran/status_pembayaran";
		$this->load->view('status_css', $data);		
	}






	
	function insert_data_konfirmasi(){
		$folderbayar = './data_pembayaran/';
		$config['upload_path'] = $folderbayar;
		$config['allowed_types'] = 'png|jpg|jpeg';
		$config['max_size']	= '20480';
		$this->load->library('upload', $config);
		if ($this->upload->do_upload())
		{
			$upload_data = $this->upload->data();
			$semester_bayar = '20191'; // change the semester
			$nama = $this->input->post('nama');
			$nim = $this->input->post('nim');
			$jurusan = $this->input->post('jurusan');
			$semester = $this->input->post('semester');
			$metode = $this->input->post('metode');
			$jumlah = $this->input->post('jumlah');
			$atasnama = $this->input->post('atasnama');
			$tgltransfer = $this->input->post('tgltransfer');
			
			$temp_path_file=$upload_data['full_path'];	
			$path_image = basename($temp_path_file);
			//echo $path_image;
			//$path_image = $folderbayar.$path_image1;
			$uk = $this->input->post('uk');
			$uo = $this->input->post('uo');
			$maba = $this->input->post('maba');
			$sksujul = $this->input->post('sksujul');
			$semestercuti = $this->input->post('semestercuti');
			$modul_sem1 = $this->input->post('modul_sem1');
			$modul_sem2 = $this->input->post('modul_sem2');
			$modul_sem3 = $this->input->post('modul_sem3');
			$modul_sem4 = $this->input->post('modul_sem4');
			$modul_sem5 = $this->input->post('modul_sem5');
			$modul_sem6 = $this->input->post('modul_sem6');
			$modul_sem7 = $this->input->post('modul_sem7');
			$modul_sem8 = $this->input->post('modul_sem8');	
			$pesan = $this->input->post('pesan');
			
			
			$this->modelp->insert_konf($semester_bayar, $nama, $nim, $jurusan, $semester, $metode, $jumlah, $tgltransfer, $atasnama, $path_image, $uk, $uo, $sksujul, $semestercuti, $modul_sem1, $modul_sem2, $modul_sem3, $modul_sem4, $modul_sem5, $modul_sem6, $modul_sem7, $modul_sem8, $pesan, $maba);
			$this->show_resume($semester_bayar, $nim, $path_image);
		}
		else
		{
			echo $this->upload->display_errors();
		}	
	}

	
	
	function show_resume($semester_bayar, $nim, $path_image){
		$data['result'] = $this->modelp->get_konf_data($semester_bayar, $nim, $path_image);
		foreach($data['result'] as $rows)
		{
			$data['bayar_id'] 	= $rows['bayar_id'];		
			$data['semester_bayar'] 	= $rows['semester_bayar'];		
			$data['nim'] 	= $rows['nim'];
			$data['nama'] 	= $rows['nama'];		
			$data['jurusan'] 	= $rows['jurusan'];		
			$data['semester'] 	= $rows['semester'];		
			$data['metode'] 	= $rows['metode'];		
			$data['jumlah'] 	= $rows['jumlah'];		
			$data['atasnama'] 	= $rows['atasnama'];		
			$data['tgltransfer'] 	= $rows['tgltransfer'];		
			$data['path_image'] 	= $rows['path_image'];		
			$data['uk'] 	= $rows['uk'];		
			$data['uo'] 	= $rows['uo'];		
			$data['maba'] 	= $rows['maba'];		
			$data['sksujul'] 	= $rows['sksujul'];				
			$data['semestercuti'] 	= $rows['semestercuti'];		
			$data['modul_sem1'] 	= $rows['modul_sem1'];		
			$data['modul_sem2'] 	= $rows['modul_sem2'];		
			$data['modul_sem3'] 	= $rows['modul_sem3'];		
			$data['modul_sem4'] 	= $rows['modul_sem4'];		
			$data['modul_sem5'] 	= $rows['modul_sem5'];		
			$data['modul_sem6'] 	= $rows['modul_sem6'];		
			$data['modul_sem7'] 	= $rows['modul_sem7'];		
			$data['modul_sem8'] 	= $rows['modul_sem8'];		
			$data['pesan'] 	= $rows['pesan'];		
		}

		$data['isi'] = "konfirmasi/show_konf";				
		$this->load->view('konfirmasi/summary', $data);	
	}

	function hapus_data(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$this->modelp->hapus_data($data['id']);
			redirect('pembayaran/homebayar');
		}
		else{
			echo $this->upload->display_errors();
		}
	}

	function detail_bayar(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->getBayar_id($data['id']);
			foreach($data['result'] as $rows)
			{
				$data['bayar_id'] 	= $rows['bayar_id'];		
				$data['semester_bayar'] 	= $rows['semester_bayar'];		
				$data['nim'] 	= $rows['nim'];
				$data['nama'] 	= $rows['nama'];		
				$data['jurusan'] 	= $rows['jurusan'];		
				$data['semester'] 	= $rows['semester'];		
				$data['metode'] 	= $rows['metode'];		
				$data['jumlah'] 	= $rows['jumlah'];		
				$data['atasnama'] 	= $rows['atasnama'];		
				$data['tgltransfer'] 	= $rows['tgltransfer'];		
				$data['path_image'] 	= $rows['path_image'];		
				$data['uk'] 	= $rows['uk'];		
				$data['uo'] 	= $rows['uo'];	
				$data['maba'] 	= $rows['maba'];		
				$data['sksujul'] 	= $rows['sksujul'];				
				$data['semestercuti'] 	= $rows['semestercuti'];		
				$data['modul_sem1'] 	= $rows['modul_sem1'];		
				$data['modul_sem2'] 	= $rows['modul_sem2'];		
				$data['modul_sem3'] 	= $rows['modul_sem3'];		
				$data['modul_sem4'] 	= $rows['modul_sem4'];		
				$data['modul_sem5'] 	= $rows['modul_sem5'];		
				$data['modul_sem6'] 	= $rows['modul_sem6'];		
				$data['modul_sem7'] 	= $rows['modul_sem7'];		
				$data['modul_sem8'] 	= $rows['modul_sem8'];		
				$data['pesan'] 	= $rows['pesan'];	
				$data['status'] 	= $rows['status'];		
			}

			$data['isi'] = "pembayaran/detail_bayar";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
			
		}
		else{
			echo $this->upload->display_errors();
		}
	}


	function konfirmasiBayar(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->konfirmasiBayar($data['id']);
			$temp = "pembayaran/detail_bayar/";
			redirect($temp.$data['id']);
		}
		else{
			echo $this->upload->display_errors();
		}
	}

	function ubahBayar(){
		$data['id'] = $this->uri->segment(3);
		$this->checkSession();
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->getBayar_id($data['id']);
			foreach($data['result'] as $rows)
			{
				$data['bayar_id'] 	= $rows['bayar_id'];		
				$data['semester_bayar'] 	= $rows['semester_bayar'];		
				$data['nim'] 	= $rows['nim'];
				$data['nama'] 	= $rows['nama'];		
				$data['jurusan'] 	= $rows['jurusan'];		
				$data['semester'] 	= $rows['semester'];		
				$data['metode'] 	= $rows['metode'];		
				$data['jumlah'] 	= $rows['jumlah'];		
				$data['atasnama'] 	= $rows['atasnama'];		
				$data['tgltransfer'] 	= $rows['tgltransfer'];		
				$data['path_image'] 	= $rows['path_image'];		
				$data['uk'] 	= $rows['uk'];		
				$data['uo'] 	= $rows['uo'];		
				$data['maba'] 	= $rows['maba'];	
				$data['sksujul'] 	= $rows['sksujul'];				
				$data['semestercuti'] 	= $rows['semestercuti'];		
				$data['modul_sem1'] 	= $rows['modul_sem1'];		
				$data['modul_sem2'] 	= $rows['modul_sem2'];		
				$data['modul_sem3'] 	= $rows['modul_sem3'];		
				$data['modul_sem4'] 	= $rows['modul_sem4'];		
				$data['modul_sem5'] 	= $rows['modul_sem5'];		
				$data['modul_sem6'] 	= $rows['modul_sem6'];		
				$data['modul_sem7'] 	= $rows['modul_sem7'];		
				$data['modul_sem8'] 	= $rows['modul_sem8'];		
				$data['pesan'] 	= $rows['pesan'];	
				$data['status'] 	= $rows['status'];		
			}

			$data['isi'] = "pembayaran/ubahBayar";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
			
		}
		else{
			echo $this->upload->display_errors();
		}
	}

	function ubahDataBayar(){
		
		$this->checkSession();
		if($this->session->userdata('email')){
			$bayar_id = $this->input->post('bayar_id');
			$metode = $this->input->post('metode');
			$jumlah = $this->input->post('jumlah');
			$atasnama = $this->input->post('atasnama');
			$uk = $this->input->post('uk');
			$uo = $this->input->post('uo');
			$maba = $this->input->post('maba');
			$sksujul = $this->input->post('sksujul');
			$semestercuti = $this->input->post('semestercuti');
			$modul_sem1 = $this->input->post('modul_sem1');
			$modul_sem2 = $this->input->post('modul_sem2');
			$modul_sem3 = $this->input->post('modul_sem3');
			$modul_sem4 = $this->input->post('modul_sem4');
			$modul_sem5 = $this->input->post('modul_sem5');
			$modul_sem6 = $this->input->post('modul_sem6');
			$modul_sem7 = $this->input->post('modul_sem7');
			$modul_sem8 = $this->input->post('modul_sem8');
			$data['result'] = $this->modelp->ubahDataBayar($bayar_id, $metode, $jumlah, $atasnama, $uk, $uo, $maba, $sksujul, $semestercuti, $modul_sem1, $modul_sem2, $modul_sem3, $modul_sem4, $modul_sem5, $modul_sem6, $modul_sem7, $modul_sem8);
			redirect("pembayaran/homebayar");
		}
		else{
			echo $this->upload->display_errors();
		}
	}

	function download_pembayaran(){
		if($this->session->userdata('email')){
			$data['result'] = $this->modelp->getDownloadBayarSemester();
			foreach($data['result'] as $rows)
			{
				$data['ID_SEMESTER'] 	= $rows['ID_SEMESTER'];			
			}
			$data['isi'] = "pembayaran/download_pembayaran";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);
		}
		else{
			echo $this->upload->display_errors();
		}
	}


	function download_rekapbayar() {
		$this->checkSession();
		if($this->session->userdata('email'))		
		{
			$jurusan = $this->input->post('jurusan');
			// create file name
			$semester = $this->input->post('semester'); 
			$fileName = 'pembayaran_smstr-'.$semester.'_time-'.time().'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$empInfo = $this->modelp->selectPembayaranSemester($semester);
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'bayar_id');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'semester_bayar');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'semester');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'nim');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'nama');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'jurusan');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'metode');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'jumlah');
			$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'tgltransfer');
			$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'atasnama');
			$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'uk');
			$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'uo');
			$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'sksujul');
			$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'semestercuti');
			$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'modul_sem1');
			$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'modul_sem2');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'modul_sem3');
			$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'modul_sem4');
			$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'modul_sem5');
			$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'modul_sem6');
			$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'modul_sem7');
			$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'modul_sem8');
			$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'status');
			
			// set Row
			$rowCount = 2;
			foreach ($empInfo as $element) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['bayar_id']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['semester_bayar']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['semester']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['nim']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['nama']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['jurusan']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['metode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['jumlah']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['tgltransfer']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['atasnama']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['uk']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['uo']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['sksujul']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['semestercuti']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['modul_sem1']);
				$objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['modul_sem2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['modul_sem3']);
				$objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $element['modul_sem4']);
				$objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $element['modul_sem5']);
				$objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $element['modul_sem6']);
				$objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $element['modul_sem7']);
				$objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $element['modul_sem8']);
				$objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $element['status']);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('rekapBayar/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			$temp = base_url() . "/rekapBayar/" .$fileName;
			redirect($temp); 
			
		}			
    }

}