<?php
class Momodel extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	
	function cekData($email, $passwd)
	{
		if($email == 'admin@admin.ut' && $passwd == 'c3e11e3c6f01449fc4ca9c47d9c8625a') //md5(uttaiwan)
		{
			return true;
		}

	}
	
	function cekxls() {
		$sql = "select ID_MHS,NAMA_MHS from Mahasiswa";
		$query = $this->db->query($sql);
    	return $query->result_array();
    }
	
	function cekxls1($tahun){
		$sql = "SELECT distinct Pembayaran.SEMESTER_ID, Mahasiswa.ANGKATAN_MHS, Pembayaran.MAHASISWA_ID, Mahasiswa.NIM_MHS, Mahasiswa.NAMA_MHS, Mahasiswa.TELP_MHS, Mahasiswa.NAMAFB_MHS, Mahasiswa.SEMESTER_AKTIF, Pembayaran.TGL_PEMBAYARAN, Pembayaran.conf_status, Pembayaran.atas_nm, Pembayaran.no_tlp, Pembayaran.UO, Pembayaran.UK,Pembayaran.UU FROM Pembayaran, Mahasiswa WHERE  SEMESTER_ID = '".$tahun."' and Mahasiswa.ID_MHS = Pembayaran.MAHASISWA_ID ";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekAngkatan(){
		$sql = "select distinct ANGKATAN_MHS from Mahasiswa";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekJurusan(){
		$sql = "select distinct ID_PROGSTUDI, NAMA_PROGSTUDI from Program_Studi";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxls2($tahun){
		$sql = "SELECT ID_MHS,NAMA_MHS,ALAMAT_MHS,TELP_MHS,CELLPHONE_MHS,EMAIL_MHS,TGL_LHR_MHS,TMP_LHR_MHS,AGAMA_MHS, NOREKENING_MHS,NAMAFB_MHS, ANGKATAN_MHS, AKTIF, SEMESTER_AKTIF
		FROM Mahasiswa 
		WHERE ANGKATAN_MHS='".$tahun."' ";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxls3($jurusan){
		$sql = "SELECT ID_MHS,NAMA_MHS,ALAMAT_MHS,TELP_MHS,CELLPHONE_MHS,EMAIL_MHS,TGL_LHR_MHS,TMP_LHR_MHS,AGAMA_MHS, NOREKENING_MHS,NAMAFB_MHS, ANGKATAN_MHS, AKTIF, SEMESTER_AKTIF
		FROM Mahasiswa 
		WHERE PROGSTUDI_ID='".$jurusan."' ";
		
		//$sql = "insert into ebook (email, kategori, judul, pengarang, penerbit, tahun, sinopsis, path_ebook, tgl_upload,status_ebook) values('".$email."','".$judul."','".$kategori."', '".$pengarang."', '".$penerbit."', '".$tahun."', '".$sinopsis."', '".$path_file."',now(),1)";
		//$query=$this->db->query($sql);
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	//=====================================================
	// Tutor
	//=====================================================
	
	function selectAllTutor(){
//        return 'masuk';
        $sql = "SELECT t.*, mk1.NAMA_MK as NM_MK1, mk2.NAMA_MK as NM_MK2 FROM Tutor t left join Mata_Kuliah mk1 on t.ID_MK1 = mk1.ID_MK left JOIN Mata_Kuliah mk2 on t.ID_MK2 = mk2.ID_MK order by t.SEMESTER_DAFTAR desc, t.NM_TUTOR ASC";
        $query = $this->db->query($sql);
//        return $query;
        return $query->result_array();

	}
	
	function getID_TUTOR($ID_TUTOR){
		$sql = "SELECT t.*, mk1.NAMA_MK as NM_MK1, mk2.NAMA_MK as NM_MK2 FROM Tutor t left join Mata_Kuliah mk1 on t.ID_MK1 = mk1.ID_MK left JOIN Mata_Kuliah mk2 on t.ID_MK2 = mk2.ID_MK WHERE ID_TUTOR='".$ID_TUTOR."'";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function hapusID_TUTOR($id){
		$sql = "DELETE from Tutor where ID_TUTOR = '".$id."' ";
		$query = $this->db->query($sql);
		if ( $this->db->affected_rows() == '1' ) {return TRUE;}
		else {return FALSE;}
	}
	
	function updateTutor($ID_TUTOR,$SEMESTER_DAFTAR,$NM_TUTOR,$ALAMAT_TUTOR,$HP_TUTOR,$EMAIL_TUTOR,$REK_TUTOR,$ID_MK1,$ID_MK2,$DISTRIK_TUTOR,$KOTA_TUTOR,$KDPOS_TUTOR,$FB_TUTOR,$NPWP_TUTOR,$PANGKAT_TUTOR,$RIWAYAT_HIDUP_TUTOR,$IJAZAH_TUTOR,$TRANSKIP_TUTOR,$TRANSKIP_S2_TUTOR,$SKM_TUTOR,$FOTO_TUTOR){
		$sql = "UPDATE Tutor
		SET SEMESTER_DAFTAR='".$SEMESTER_DAFTAR."', NM_TUTOR='".$NM_TUTOR."', ALAMAT_TUTOR = '".$ALAMAT_TUTOR.
		"', HP_TUTOR = '".$HP_TUTOR."', EMAIL_TUTOR = '".$EMAIL_TUTOR."', REK_TUTOR = '".$REK_TUTOR.
		"', ID_MK1 = '".$ID_MK1."', ID_MK2 = '".$ID_MK2."', DISTRIK_TUTOR = '".$DISTRIK_TUTOR.
		"', KOTA_TUTOR = '".$KOTA_TUTOR."', KDPOS_TUTOR = '".$KDPOS_TUTOR."', FB_TUTOR = '".$FB_TUTOR.
		"', NPWP_TUTOR = '".$NPWP_TUTOR."', PANGKAT_TUTOR = '".$PANGKAT_TUTOR."', RIWAYAT_HIDUP_TUTOR = '".$RIWAYAT_HIDUP_TUTOR.
		"', IJAZAH_TUTOR = '".$IJAZAH_TUTOR."', TRANSKIP_TUTOR = '".$TRANSKIP_TUTOR."', TRANSKIP_S2_TUTOR = '".$TRANSKIP_S2_TUTOR.
		"', SKM_TUTOR = '".$SKM_TUTOR."', FOTO_TUTOR = '".$FOTO_TUTOR.
		"' WHERE ID_TUTOR='".$ID_TUTOR."'";
		$query = $this->db->query($sql);
		if ( $this->db->affected_rows() == '1' ) {return TRUE;}
		else {return FALSE;}
	}
	
	function inserttutor($SEMESTER_DAFTAR, $NM_TUTOR, $ALAMAT_TUTOR, $DISTRIK_TUTOR, $KOTA_TUTOR, $KDPOS_TUTOR, $HP_TUTOR, $EMAIL_TUTOR, $FB_TUTOR, $REK_TUTOR, $NPWP_TUTOR, $PANGKAT_TUTOR, $RIWAYAT_HIDUP_TUTOR, $IJAZAH_TUTOR, $TRANSKIP_TUTOR, $TRANSKIP_S2_TUTOR, $SKM_TUTOR, $FOTO_TUTOR, $ID_MK1, $ID_MK2){
		$sql = "insert into Tutor(SEMESTER_DAFTAR, NM_TUTOR, ALAMAT_TUTOR, DISTRIK_TUTOR, KOTA_TUTOR, KDPOS_TUTOR, HP_TUTOR, EMAIL_TUTOR, FB_TUTOR, REK_TUTOR, NPWP_TUTOR, PANGKAT_TUTOR, RIWAYAT_HIDUP_TUTOR, IJAZAH_TUTOR, TRANSKIP_TUTOR, TRANSKIP_S2_TUTOR, SKM_TUTOR, FOTO_TUTOR, ID_MK1, ID_MK2) values ('".$SEMESTER_DAFTAR."', '".$NM_TUTOR."', '".$ALAMAT_TUTOR."', '".$DISTRIK_TUTOR."', '".$KOTA_TUTOR."', '".$KDPOS_TUTOR."', '".$HP_TUTOR."', '".$EMAIL_TUTOR."', '".$FB_TUTOR."', '".$REK_TUTOR."', '".$NPWP_TUTOR."', '".$PANGKAT_TUTOR."', '".$RIWAYAT_HIDUP_TUTOR."', '".$IJAZAH_TUTOR."', '".$TRANSKIP_TUTOR."', '".$TRANSKIP_S2_TUTOR."', '".$SKM_TUTOR."', '".$FOTO_TUTOR."', '".$ID_MK1."', '".$ID_MK2."')";
		$query=$this->db->query($sql);
	}
	
	function cekSemesterTutor(){
		$sql = "select distinct SEMESTER_DAFTAR from Tutor";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxlsTutor($tahun){
		$sql = "SELECT * from Tutor";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxls_tutor($SEMESTER_DAFTAR){
		$sql = "SELECT SEMESTER_DAFTAR, NM_TUTOR, ALAMAT_TUTOR, DISTRIK_TUTOR, KOTA_TUTOR, KDPOS_TUTOR, HP_TUTOR, EMAIL_TUTOR, FB_TUTOR, REK_TUTOR, NPWP_TUTOR, PANGKAT_TUTOR, RIWAYAT_HIDUP_TUTOR, IJAZAH_TUTOR, TRANSKIP_TUTOR, TRANSKIP_S2_TUTOR, SKM_TUTOR, FOTO_TUTOR, ID_MK1, ID_MK2, waktu
		FROM tutor
		WHERE SEMESTER_DAFTAR='".$SEMESTER_DAFTAR."' ";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function getMatKul()
	{
		$sql = "select ID_MK,NAMA_MK from Mata_Kuliah where 
		(ID_MK='140' || ID_MK='129' || ID_MK='116' || ID_MK='104' || ID_MK='25' || ID_MK='26' || ID_MK='32' || ID_MK='33' || ID_MK='5')  ORDER by PROGSTUDI_ID, NAMA_MK ASC";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	//=====================================================
	// Konfirmasi Pembayaran
	//=====================================================
	function insert_konf($semester_bayar, $nama, $nim, $jurusan, $semester, $metode, $jumlah, $tgltransfer, $atasnama, $path_image, $uk, $uo, $sksujul, $semestercuti, $modul_sem1, $modul_sem2, $modul_sem3, $modul_sem4, $modul_sem5, $modul_sem6, $modul_sem7, $modul_sem8, $pesan, $maba){
		$sql = "insert into bayar2(semester_bayar, nama, nim, jurusan, semester, metode, jumlah, tgltransfer, atasnama, path_image, uk, uo, sksujul, semestercuti, modul_sem1, modul_sem2, modul_sem3, modul_sem4, modul_sem5, modul_sem6, modul_sem7, modul_sem8, pesan,status, maba) values ('".$semester_bayar."', '".$nama."', '".$nim."', '".$jurusan."', '".$semester."', '".$metode."', '".$jumlah."', '".$tgltransfer."', '".$atasnama."', '".$path_image."', '".$uk."', '".$uo."', '".$sksujul."',  '".$semestercuti."', '".$modul_sem1."', '".$modul_sem2."', '".$modul_sem3."', '".$modul_sem4."', '".$modul_sem5."', '".$modul_sem6."', '".$modul_sem7."', '".$modul_sem8."', '".$pesan."',0, '".$maba."')";
		$query=$this->db->query($sql);
	}

	
	function get_konf_data($semester_bayar, $nim, $path_image){
		$sql = "SELECT *
		FROM bayar2
		WHERE semester_bayar='".$semester_bayar."' 
		and nim='".$nim."'
		and path_image='".$path_image."'";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function getbayar($semester_bayar){
		$sql = "SELECT * 
		FROM bayar2
		WHERE semester_bayar='".$semester_bayar."'";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}

	function selectPembayaran(){
		$sql = "SELECT *
		FROM bayar2
		order by bayar_id desc";
		$query = $this->db->query($sql);
    	return $query->result_array();

	}

	function selectPembayaranSemester($semester){
		$sql = "SELECT *
		FROM bayar2
		WHERE semester_bayar = '".$semester."'
		order by status asc";
		$query = $this->db->query($sql);
    	return $query->result_array();

	}

	function getBayar_id($bayar_id){
		$sql = "SELECT *
		FROM bayar2
		WHERE bayar_id='".$bayar_id."'";
		$query = $this->db->query($sql);
    	return $query->result_array();

	}

	function hapus_data($id){
		$sql = "DELETE from bayar2 where bayar_id = '".$id."' ";
		$query = $this->db->query($sql);
	}

	function konfirmasiBayar($id){
		$sql = "UPDATE bayar2 SET status=1 WHERE bayar_id='".$id."'";
		$query = $this->db->query($sql);
	}

	function ubahDataBayar($id, $metode, $jumlah, $atasnama, $uk, $uo, $maba, $sksujul, $semestercuti, $modul_sem1, $modul_sem2, $modul_sem3, $modul_sem4, $modul_sem5, $modul_sem6, $modul_sem7, $modul_sem8){
		$sql = "UPDATE bayar2 
		SET metode='".$metode."', jumlah='".$jumlah."', atasnama = '".$atasnama."', uk = '".$uk."', uo = '".$uo."', maba = '".$maba."'
		, sksujul = '".$sksujul."', semestercuti = '".$semestercuti."'
		, modul_sem1 = '".$modul_sem1."', modul_sem2 = '".$modul_sem2."'
		, modul_sem3 = '".$modul_sem3."', modul_sem4 = '".$modul_sem4."'
		, modul_sem5 = '".$modul_sem5."', modul_sem6 = '".$modul_sem6."'
		, modul_sem7 = '".$modul_sem7."', modul_sem8 = '".$modul_sem8."'
		WHERE bayar_id='".$id."'";
		$query = $this->db->query($sql);
	}

	function getDownloadBayarSemester(){
		$sql = "SELECT *
		FROM Semester";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function selectAllMahasiswa(){
		// $sql = "SELECT * FROM 'Mahasiswa' JOIN 'Tempat_Ujian' ON 'Mahasiswa.ID_TEMPATUJIAN' = 'Tempat_Ujian.ID_TEMPATUJIN'";
		$this->db->select('*');
		$this->db->from('Mahasiswa');
		$this->db->join('Tempat_Ujian', 'Tempat_Ujian.ID_TEMPATUJIAN = Mahasiswa.ID_TEMPATUJIAN');
		$query=$this->db->get();
		$data=$query->result_array();
    	return $data;
	}

	function selectMahasiswaById($id_mhs)
	{
		$this->db->select('*');
		$this->db->from('Mahasiswa');
		$this->db->where(array('Mahasiswa.ID_MHS' => $id_mhs));
		$this->db->join('Tempat_Ujian', 'Tempat_Ujian.ID_TEMPATUJIAN = Mahasiswa.ID_TEMPATUJIAN');
		$this->db->join('Program_Studi', 'Mahasiswa.PROGSTUDI_ID = Program_Studi.ID_PROGSTUDI');
		$query=$this->db->get();
		$data=$query->result_array();
		return $data;
	}

	function deleteMahasiswaById($id_mhs)
	{
		$sql = "DELETE from Mahasiswa where ID_MHS = '".$id_mhs."' ";
		$query = $this->db->query($sql);
	}

	function insertMahasiswa($data)	{
		// $a = chr(rand(65,90));
		// $b = chr(rand(65,90));
		// $c = chr(rand(65,90));
		// $d = chr(rand(65,90));
		// $id_mhs = ''.$a.''.$b.''.$c.''.$d.''.rand(0, 9).''.rand(0, 9).''.rand(0, 9).''.rand(0, 9);
		// $created = date("Y-m-d H:i:s");
		// $sql = "INSERT INTO Mahasiswa (ID_MHS, NIM_MHS, NAMA_MHS, ALAMAT_MHS, DISTRICT_MHS, KABKOT_MHS, KODEPOS_MHS, TELP_MHS, CELLPHONE_MHS, EMAIL_MHS, TGL_LHR_MHS, TMP_LHR_MHS, AGAMA_MHS, PROGSTUDI_ID, KDUPBJJ_MHS, JK_MHS, WN_MHS, PEKERJAAN_MHS, STKWN_MHS, JENJANGPDK_MHS, JURUSAN_MHS, THNIJAZAH_MHS, NAMAIBU_MHS, NOREKENING_MHS, BANK_MHS, ANGKATAN_MHS, NAMAFB_MHS, AKTIF, TIME, SEMESTER_AKTIF, ID_TEMPATUJIAN) VALUES ('".$id_mhs."', '".$data['nim_mhs']."', '".$data['nama_mhs']."', '".$data['alamat_mhs']."', '".$data['district_mhs']."', '".$data['kabkot_mhs']."', '".$data['kodepos_mhs']."', '".$data['telp_mhs']."', '".$data['cellphone_mhs']."', '".$data['email_mhs']."', '".$data['tgl_lhr_mhs']."', '".$data['tmp_lhr_mhs']."', '".$data['agama_mhs']."', '".$data['progstudi_id']."', '".$data['kdupbjj_mhs']."', '".$data['jk_mhs']."', '".$data['wn_mhs']."', '".$data['pekerjaan_mhs'].", '".$data['stkwn_mhs']."', '".$data['jenjangpdk_mhs']."', '".$data['jurusan_mhs']."', '".$data['thnijazah_mhs']."', '".$data['namaibu_mhs']."', '".$data['norekening_mhs']."', '".$data['bank_mhs']."', '".$data['angkatan_mhs']."', '".$data['namafb_mhs']."', '".$data['aktif']."', '".$created."', '".$data['semester_aktif']."', '".$data['id_tempatujian']."')";

		$query = $this->db->query($sql);
		return $query;
	}
}
?>
