<?pHP_TUTOR
class Momodel extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	
	function cekData($EMAIL_TUTOR, $passwd)
	{
		if($EMAIL_TUTOR == 'admin@admin.ut' && $passwd == 'c3e11e3c6f01449fc4ca9c47d9c8625a') //md5(uttaiwan)
		{
			return true;
		}

	}
	
	function cekxls() {
		$sql = "select ID_MHS,NM_TUTOR_MHS from Mahasiswa";
		$query = $this->db->query($sql);
    	return $query->result_array();
    }
	
	function cekxls1($tahun){
		$sql = "SELECT distinct Pembayaran.SEMESTER_DAFTAR_ID, Mahasiswa.ANGKATAN_MHS, Pembayaran.MAHASISWA_ID, Mahasiswa.NIM_MHS, Mahasiswa.NM_TUTOR_MHS, Mahasiswa.TELP_MHS, Mahasiswa.NM_TUTORFB_TUTOR_MHS, Mahasiswa.SEMESTER_DAFTAR_AKTIF, Pembayaran.TGL_PEMBAYARAN, Pembayaran.conf_status, Pembayaran.atas_nm, Pembayaran.no_tlp, Pembayaran.UO, Pembayaran.UK,Pembayaran.UU FROM Pembayaran, Mahasiswa WHERE  SEMESTER_DAFTAR_ID = '".$tahun."' and Mahasiswa.ID_MHS = Pembayaran.MAHASISWA_ID ";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekAngkatan(){
		$sql = "select distinct ANGKATAN_MHS from Mahasiswa";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekJurusan(){
		$sql = "select distinct ID_PROGSTUDI, NM_TUTOR_PROGSTUDI from Program_Studi";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxls2($tahun){
		$sql = "SELECT ID_MHS,NM_TUTOR_MHS,ALAMAT_TUTOR_MHS,TELP_MHS,CELLPHONE_MHS,EMAIL_TUTOR_MHS,TGL_LHR_MHS,TMP_LHR_MHS,AGAMA_MHS, NOREK_TUTORENING_MHS,NM_TUTORFB_TUTOR_MHS, ANGKATAN_MHS, AKTIF, SEMESTER_DAFTAR_AKTIF
		FROM Mahasiswa 
		WHERE ANGKATAN_MHS='".$tahun."' ";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxls3($jurusan){
		$sql = "SELECT ID_MHS,NM_TUTOR_MHS,ALAMAT_TUTOR_MHS,TELP_MHS,CELLPHONE_MHS,EMAIL_TUTOR_MHS,TGL_LHR_MHS,TMP_LHR_MHS,AGAMA_MHS, NOREK_TUTORENING_MHS,NM_TUTORFB_TUTOR_MHS, ANGKATAN_MHS, AKTIF, SEMESTER_DAFTAR_AKTIF
		FROM Mahasiswa 
		WHERE PROGSTUDI_ID='".$jurusan."' ";
		
		//$sql = "insert into ebook (EMAIL_TUTOR, kategori, judul, pengarang, penerbit, tahun, sinopsis, path_ebook, tgl_upload,status_ebook) values('".$EMAIL_TUTOR."','".$judul."','".$kategori."', '".$pengarang."', '".$penerbit."', '".$tahun."', '".$sinopsis."', '".$path_file."',now(),1)";
		//$query=$this->db->query($sql);
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	//=====================================================
	// Tutor
	//=====================================================
	
	function selectAllTutor(){
		$sql = "SELECT * from tutor order by SEMESTER_DAFTAR desc, NM_TUTOR asc";
		$query = $this->db->query($sql);
    	return $query->result_array();

	}
	
	function getID_TUTOR($ID_TUTOR){
		$sql = "SELECT * from tutor
		WHERE ID_TUTOR='".$ID_TUTOR."'";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function hapusID_TUTOR($id){
		$sql = "DELETE from tutor where ID_TUTOR = '".$id."' ";
		$query = $this->db->query($sql);
		if ( $this->db->affected_rows() == '1' ) {return TRUE;}
		else {return FALSE;}
	}
	
	function updateTutor($ID_TUTOR,$SEMESTER_DAFTAR,$NM_TUTOR,$ALAMAT_TUTOR,$HP_TUTOR,$EMAIL_TUTOR,$REK_TUTOR,$ID_MK1,$ID_MK2,$DISTRIK_TUTOR,$KOTA_TUTOR,$KDPOS_TUTOR,$FB_TUTOR,$NPWP_TUTOR,$PANGKAT_TUTOR,$RIWAYAT_HIDUP_TUTOR,$IJAZAH_TUTOR,$TRANSKIP_TUTOR,$TRANSKIP_S2_TUTOR,$SKM_TUTOR,$FOTO_TUTOR){
		$sql = "UPDATE tutor 
		SET SEMESTER_DAFTAR='".$SEMESTER_DAFTAR."', NM_TUTOR='".$NM_TUTOR."', ALAMAT_TUTOR = '".$ALAMAT_TUTOR.
		"', HP_TUTOR = '".$HP_TUTOR."', EMAIL_TUTOR = '".$EMAIL_TUTOR."', REK_TUTOR = '".$REK_TUTOR.
		"', ID_MK1 = '".$ID_MK1."', ID_MK2 = '".$ID_MK2."', DISTRIK_TUTOR = '".$DISTRIK_TUTOR.
		"', KOTA_TUTOR = '".$KOTA_TUTOR."', KDPOS_TUTOR = '".$KDPOS_TUTOR."', FB_TUTOR = '".$FB_TUTOR.
		"', NPWP_TUTOR = '".$NPWP_TUTOR."', PANGKAT_TUTOR = '".$PANGKAT_TUTOR."', RIWAYAT_HIDUP_TUTOR = '".$RIWAYAT_HIDUP_TUTOR.
		"', IJAZAH_TUTOR = '".$IJAZAH_TUTOR."', TRANSKIP_TUTOR = '".$TRANSKIP_TUTOR."', TRANSKIP_S2_TUTOR = '".$TRANSKIP_S2_TUTOR.
		"', SKM_TUTOR = '".$SKM_TUTOR."', FOTO_TUTOR = '".$FOTO_TUTOR.
		"' WHERE ID_TUTOR='".$ID_TUTOR."'";
		$query = $this->db->query($sql);
		if ( $this->db->affected_rows() == '1' ) {return TRUE;}
		else {return FALSE;}
	}
	
	function inserttutor($SEMESTER_DAFTAR, $NM_TUTOR, $ALAMAT_TUTOR, $DISTRIK_TUTOR, $KOTA_TUTOR, $KDPOS_TUTOR, $HP_TUTOR, $EMAIL_TUTOR, $FB_TUTOR, $REK_TUTOR, $NPWP_TUTOR, $PANGKAT_TUTOR, $RIWAYAT_HIDUP_TUTOR, $IJAZAH_TUTOR, $TRANSKIP_TUTOR, $TRANSKIP_S2_TUTOR, $SKM_TUTOR, $FOTO_TUTOR, $ID_MK1, $ID_MK2){
		$sql = "insert into tutor(SEMESTER_DAFTAR, NM_TUTOR, ALAMAT_TUTOR, DISTRIK_TUTOR, KOTA_TUTOR, KDPOS_TUTOR, HP_TUTOR, EMAIL_TUTOR, FB_TUTOR, REK_TUTOR, NPWP_TUTOR, PANGKAT_TUTOR, RIWAYAT_HIDUP_TUTOR, IJAZAH_TUTOR, TRANSKIP_TUTOR, TRANSKIP_S2_TUTOR, SKM_TUTOR, FOTO_TUTOR, ID_MK1, ID_MK2) values ('".$SEMESTER_DAFTAR."', '".$NM_TUTOR."', '".$ALAMAT_TUTOR."', '".$DISTRIK_TUTOR."', '".$KOTA_TUTOR."', '".$KDPOS_TUTOR."', '".$HP_TUTOR."', '".$EMAIL_TUTOR."', '".$FB_TUTOR."', '".$REK_TUTOR."', '".$NPWP_TUTOR."', '".$PANGKAT_TUTOR."', '".$RIWAYAT_HIDUP_TUTOR."', '".$IJAZAH_TUTOR."', '".$TRANSKIP_TUTOR."', '".$TRANSKIP_S2_TUTOR."', '".$SKM_TUTOR."', '".$FOTO_TUTOR."', '".$ID_MK1."', '".$ID_MK2."')";
		$query=$this->db->query($sql);
	}
	
	function cekSEMESTER_DAFTARTutor(){
		$sql = "select distinct SEMESTER_DAFTAR from tutor";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxlsTutor($tahun){
		$sql = "SELECT * from tutor";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function cekxls_tutor($SEMESTER_DAFTAR){
		$sql = "SELECT SEMESTER_DAFTAR, NM_TUTOR, ALAMAT_TUTOR, DISTRIK_TUTOR, KOTA_TUTOR, KDPOS_TUTOR, HP_TUTOR, EMAIL_TUTOR, FB_TUTOR, REK_TUTOR, NPWP_TUTOR, PANGKAT_TUTOR, RIWAYAT_HIDUP_TUTOR, IJAZAH_TUTOR, TRANSKIP_TUTOR, TRANSKIP_S2_TUTOR, SKM_TUTOR, FOTO_TUTOR, ID_MK1, ID_MK2, waktu
		FROM tutor
		WHERE SEMESTER_DAFTAR='".$SEMESTER_DAFTAR."' ";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	
	
	
	//=====================================================
	// Konfirmasi Pembayaran
	//=====================================================
	function insert_konf($SEMESTER_DAFTAR_bayar, $NM_TUTOR, $nim, $jurusan, $SEMESTER_DAFTAR, $metode, $jumlah, $tgltransfer, $atasNM_TUTOR, $path_image, $uk, $uo, $sksujul, $SEMESTER_DAFTARcuti, $modul_sem1, $modul_sem2, $modul_sem3, $modul_sem4, $modul_sem5, $modul_sem6, $modul_sem7, $modul_sem8, $pesan, $maba){
		$sql = "insert into bayar2(SEMESTER_DAFTAR_bayar, NM_TUTOR, nim, jurusan, SEMESTER_DAFTAR, metode, jumlah, tgltransfer, atasNM_TUTOR, path_image, uk, uo, sksujul, SEMESTER_DAFTARcuti, modul_sem1, modul_sem2, modul_sem3, modul_sem4, modul_sem5, modul_sem6, modul_sem7, modul_sem8, pesan,status, maba) values ('".$SEMESTER_DAFTAR_bayar."', '".$NM_TUTOR."', '".$nim."', '".$jurusan."', '".$SEMESTER_DAFTAR."', '".$metode."', '".$jumlah."', '".$tgltransfer."', '".$atasNM_TUTOR."', '".$path_image."', '".$uk."', '".$uo."', '".$sksujul."',  '".$SEMESTER_DAFTARcuti."', '".$modul_sem1."', '".$modul_sem2."', '".$modul_sem3."', '".$modul_sem4."', '".$modul_sem5."', '".$modul_sem6."', '".$modul_sem7."', '".$modul_sem8."', '".$pesan."',0, '".$maba."')";
		$query=$this->db->query($sql);
	}

	
	function get_konf_data($SEMESTER_DAFTAR_bayar, $nim, $path_image){
		$sql = "SELECT *
		FROM bayar2
		WHERE SEMESTER_DAFTAR_bayar='".$SEMESTER_DAFTAR_bayar."' 
		and nim='".$nim."'
		and path_image='".$path_image."'";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function getbayar($SEMESTER_DAFTAR_bayar){
		$sql = "SELECT * 
		FROM bayar2
		WHERE SEMESTER_DAFTAR_bayar='".$SEMESTER_DAFTAR_bayar."'";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}

	function selectPembayaran(){
		$sql = "SELECT *
		FROM bayar2
		order by bayar_id desc";
		$query = $this->db->query($sql);
    	return $query->result_array();

	}

	function selectPembayaranSEMESTER_DAFTAR($SEMESTER_DAFTAR){
		$sql = "SELECT *
		FROM bayar2
		WHERE SEMESTER_DAFTAR_bayar = '".$SEMESTER_DAFTAR."'
		order by status asc";
		$query = $this->db->query($sql);
    	return $query->result_array();

	}

	function getBayar_id($bayar_id){
		$sql = "SELECT *
		FROM bayar2
		WHERE bayar_id='".$bayar_id."'";
		$query = $this->db->query($sql);
    	return $query->result_array();

	}

	function hapus_data($id){
		$sql = "DELETE from bayar2 where bayar_id = '".$id."' ";
		$query = $this->db->query($sql);
	}

	function konfirmasiBayar($id){
		$sql = "UPDATE bayar2 SET status=1 WHERE bayar_id='".$id."'";
		$query = $this->db->query($sql);
	}

	function ubahDataBayar($id, $metode, $jumlah, $atasNM_TUTOR, $uk, $uo, $maba, $sksujul, $SEMESTER_DAFTARcuti, $modul_sem1, $modul_sem2, $modul_sem3, $modul_sem4, $modul_sem5, $modul_sem6, $modul_sem7, $modul_sem8){
		$sql = "UPDATE bayar2 
		SET metode='".$metode."', jumlah='".$jumlah."', atasNM_TUTOR = '".$atasNM_TUTOR."', uk = '".$uk."', uo = '".$uo."', maba = '".$maba."'
		, sksujul = '".$sksujul."', SEMESTER_DAFTARcuti = '".$SEMESTER_DAFTARcuti."'
		, modul_sem1 = '".$modul_sem1."', modul_sem2 = '".$modul_sem2."'
		, modul_sem3 = '".$modul_sem3."', modul_sem4 = '".$modul_sem4."'
		, modul_sem5 = '".$modul_sem5."', modul_sem6 = '".$modul_sem6."'
		, modul_sem7 = '".$modul_sem7."', modul_sem8 = '".$modul_sem8."'
		WHERE bayar_id='".$id."'";
		$query = $this->db->query($sql);
	}

	function getDownloadBayarSEMESTER_DAFTAR(){
		$sql = "SELECT *
		FROM SEMESTER_DAFTAR";
		$query = $this->db->query($sql);
    	return $query->result_array();
	}
	
	function selectAllMahasiswa(){
		// $sql = "SELECT * FROM 'Mahasiswa' JOIN 'Tempat_Ujian' ON 'Mahasiswa.ID_TEMPATUJIAN' = 'Tempat_Ujian.ID_TEMPATUJIN'";
		$this->db->select('*');
		$this->db->from('Mahasiswa');
		$this->db->join('Tempat_Ujian', 'Tempat_Ujian.ID_TEMPATUJIAN = Mahasiswa.ID_TEMPATUJIAN');
		$query=$this->db->get();
		$data=$query->result_array();
    	return $data;
	}

	function selectMahasiswaById($id_mhs)
	{
		$this->db->select('*');
		$this->db->from('Mahasiswa');
		$this->db->where(array('Mahasiswa.ID_MHS' => $id_mhs));
		$this->db->join('Tempat_Ujian', 'Tempat_Ujian.ID_TEMPATUJIAN = Mahasiswa.ID_TEMPATUJIAN');
		$this->db->join('Program_Studi', 'Mahasiswa.PROGSTUDI_ID = Program_Studi.ID_PROGSTUDI');
		$query=$this->db->get();
		$data=$query->result_array();
		return $data;
	}

	function deleteMahasiswaById($id_mhs)
	{
		$sql = "DELETE from Mahasiswa where ID_MHS = '".$id_mhs."' ";
		$query = $this->db->query($sql);
	}

	function insertMahasiswa($data)	{
		// $a = chr(rand(65,90));
		// $b = chr(rand(65,90));
		// $c = chr(rand(65,90));
		// $d = chr(rand(65,90));
		// $id_mhs = ''.$a.''.$b.''.$c.''.$d.''.rand(0, 9).''.rand(0, 9).''.rand(0, 9).''.rand(0, 9);
		// $created = date("Y-m-d H:i:s");
		// $sql = "INSERT INTO Mahasiswa (ID_MHS, NIM_MHS, NM_TUTOR_MHS, ALAMAT_TUTOR_MHS, DISTRIK_TUTOR_MHS, KABKOT_MHS, KDPOS_TUTOR_MHS, TELP_MHS, CELLPHONE_MHS, EMAIL_TUTOR_MHS, TGL_LHR_MHS, TMP_LHR_MHS, AGAMA_MHS, PROGSTUDI_ID, KDUPBJJ_MHS, JK_MHS, WN_MHS, PEKERJAAN_MHS, STKWN_MHS, JENJANGPDK_MHS, JURUSAN_MHS, THNIJAZAH_MHS, NM_TUTORIBU_MHS, NOREK_TUTORENING_MHS, BANK_MHS, ANGKATAN_MHS, NM_TUTORFB_TUTOR_MHS, AKTIF, TIME, SEMESTER_DAFTAR_AKTIF, ID_TEMPATUJIAN) VALUES ('".$id_mhs."', '".$data['nim_mhs']."', '".$data['NM_TUTOR_mhs']."', '".$data['ALAMAT_TUTOR_mhs']."', '".$data['DISTRIK_TUTOR_mhs']."', '".$data['kabkot_mhs']."', '".$data['KDPOS_TUTOR_mhs']."', '".$data['telp_mhs']."', '".$data['cellphone_mhs']."', '".$data['EMAIL_TUTOR_mhs']."', '".$data['tgl_lhr_mhs']."', '".$data['tmp_lhr_mhs']."', '".$data['agama_mhs']."', '".$data['progstudi_id']."', '".$data['kdupbjj_mhs']."', '".$data['jk_mhs']."', '".$data['wn_mhs']."', '".$data['pekerjaan_mhs'].", '".$data['stkwn_mhs']."', '".$data['jenjangpdk_mhs']."', '".$data['jurusan_mhs']."', '".$data['thnijazah_mhs']."', '".$data['NM_TUTORibu_mhs']."', '".$data['noREK_TUTORening_mhs']."', '".$data['bank_mhs']."', '".$data['angkatan_mhs']."', '".$data['NM_TUTORFB_TUTOR_mhs']."', '".$data['aktif']."', '".$created."', '".$data['SEMESTER_DAFTAR_aktif']."', '".$data['id_tempatujian']."')";

		$query = $this->db->query($sql);
		return $query;
	}
}
?>